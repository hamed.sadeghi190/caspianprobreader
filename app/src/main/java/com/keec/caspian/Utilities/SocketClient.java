package com.keec.caspian.Utilities;

import com.keec.caspian.EventBusModels.MessageEvent;
import com.keec.caspian.EventBusModels.SocketConnectionLost;
import com.keec.caspian.EventBusModels.actionResult;
import com.koushikdutta.async.AsyncServer;
import com.koushikdutta.async.AsyncSocket;
import com.koushikdutta.async.ByteBufferList;
import com.koushikdutta.async.DataEmitter;
import com.koushikdutta.async.Util;
import com.koushikdutta.async.callback.CompletedCallback;
import com.koushikdutta.async.callback.ConnectCallback;
import com.koushikdutta.async.callback.DataCallback;

import org.greenrobot.eventbus.EventBus;

import java.net.InetSocketAddress;

import ParsingCommonFiles.Parsing.ConstDefines;

public class SocketClient {
    private String host;
    private int port;
    private AsyncSocket appSocket;
    private boolean Connected = false;

    public SocketClient(String host, int port) {
        this.host = host;
        this.port = port;
        Connect();
    }

    private void Connect() {
        AsyncServer.getDefault().connectSocket(new InetSocketAddress(host, port), new ConnectCallback() {
            @Override
            public void onConnectCompleted(Exception ex, final AsyncSocket socket) {
                appSocket = socket;
                if (ex == null) {
                    Connected = true;
                }
                EventBus.getDefault().post(new actionResult(Connected));
            }
        });
    }

    public boolean IsConnected()
    {
        return Connected;
    }
    public void SendCommand(byte[] Command, final String CommandType, final ConstDefines.MeterTypes meterType) {
        if (!Connected)
            return;
        Util.writeAll(appSocket, Command, new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                if (ex != null) throw new RuntimeException(ex);
                System.out.println("[Client] Successfully wrote message");
            }
        });
        appSocket.setDataCallback(new DataCallback() {
            @Override
            public void onDataAvailable(DataEmitter emitter, ByteBufferList bb) {
                String result = new String(bb.getAllByteArray());
                EventBus.getDefault().post(new MessageEvent(result, CommandType,meterType));
            }
        });


        appSocket.setClosedCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                if (ex != null)    EventBus.getDefault().post(new SocketConnectionLost());
                System.out.println("[Client] Successfully closed connection");
            }
        });

        appSocket.setEndCallback(new CompletedCallback() {
            @Override
            public void onCompleted(Exception ex) {
                if (ex != null)
                {
                    EventBus.getDefault().post(new SocketConnectionLost());
                }
                System.out.println("[Client] Successfully end connection");
            }
        });

    }


}
