package com.keec.caspian.Utilities;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;

public class AppController extends Application {

    public static Typeface IranBold, Iran;
    Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        Iran = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile.ttf");
        IranBold = Typeface.createFromAsset(getAssets(), "fonts/IRANSansMobile_Bold.ttf");
    }
}
