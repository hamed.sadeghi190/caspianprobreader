package com.keec.caspian;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import com.keec.caspian.EventBusModels.MessageEvent;
import org.greenrobot.eventbus.EventBus;

public class WifiReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @Override
    public void onReceive(Context context, Intent intent) {

        NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
        if(info != null && info.isConnected()) {
            EventBus.getDefault().post(new MessageEvent("Connected", "Connected",null));
        }
        else
        {
            EventBus.getDefault().post(new MessageEvent("NotConnected", "NotConnected",null));
        }
    }
}