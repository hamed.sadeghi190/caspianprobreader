package com.keec.caspian.Application.Dialogs;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;


public class BaseAlertDialog extends AlertDialog {
    protected BaseAlertDialog(@NonNull Context context) {
        super(context);
    }

    public void CloseAlert() {
        dismiss();

    }
    public void ShowAlert() {
        show();

    }


}
