package com.keec.caspian.Application.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.keec.caspian.Constants.CommandType;
import com.keec.caspian.Constants.Setting;
import com.keec.caspian.EventBusModels.MessageEvent;
import com.keec.caspian.EventBusModels.SocketConnectionLost;
import com.keec.caspian.EventBusModels.actionResult;
import com.keec.caspian.R;
import com.keec.caspian.Utilities.SocketClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import ParsingCommonFiles.Meters._ELSTER_A1350;
import ParsingCommonFiles.Meters._ELSTER_A220;
import ParsingCommonFiles.Meters._HEX_400;
import ParsingCommonFiles.Meters._IRANTEK_34;
import ParsingCommonFiles.Meters._JAM_200;
import ParsingCommonFiles.Meters._JAM_200T;
import ParsingCommonFiles.Meters._JAM_300;
import ParsingCommonFiles.Meters._JAM_300_newVersion2Tavanir;
import ParsingCommonFiles.Meters._Meter_Base;
import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterSpecialValuesStruct;
import ParsingCommonFiles.Parsing.MeterValuesStruct;
import ParsingCommonFiles.Parsing.ParseReceivedData;
import androidx.appcompat.app.AppCompatActivity;
import mehdi.sakout.fancybuttons.FancyButton;

import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.ELSTER_A1350;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.ELSTER_A220;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.HEX_400;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.IRANTEK_34;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.JAM_200;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.JAM_200T;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.JAM_300;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.JAM_300_newVersion2Tavanir;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.UNDEFINED;

public class ConnectActivity extends AppCompatActivity {

    TextView txt_wifi_name;
    ImageView img_wifi;
    FancyButton btn_Connect;
    LottieAnimationView lot_meter;
    RelativeLayout ly_data, ly_wifi;
    HorizontalScrollView scv_top;
    SocketClient socketClient;
    private final ParseReceivedData parser = new ParseReceivedData();
    public static String allMeterReadData = "";
    MeterValuesStruct meterValuesStruct;
    MeterSpecialValuesStruct meterSpecialValuesStruct;

    String counterName;
    ProgressDialog progressDialog;
    TextView lbl_serial, lbl_maxi_meter, lbl_reactive, lbl_low_load, lbl_mid_load, lbl_high_load, lbl_date, lbl_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_connect);

        initControls();
        BindControlsEvent();
        CheckWifi();
    }

    private void initControls() {
        meterValuesStruct = new MeterValuesStruct();
        meterSpecialValuesStruct = new MeterSpecialValuesStruct();

        txt_wifi_name = findViewById(R.id.txt_wifi_name);
        img_wifi = findViewById(R.id.img_wifi);
        btn_Connect = findViewById(R.id.btn_Connect);
        lot_meter = findViewById(R.id.lot_meter);
        ly_data = findViewById(R.id.ly_data);
        ly_wifi = findViewById(R.id.ly_wifi);
        scv_top = findViewById(R.id.scv_top);
        lbl_serial = findViewById(R.id.tx_serial);
        lbl_maxi_meter = findViewById(R.id.tx_maximeter);
        lbl_reactive = findViewById(R.id.tx_reactive);
        lbl_low_load = findViewById(R.id.tx_lowload);
        lbl_mid_load = findViewById(R.id.tx_MiddleLoad);
        lbl_high_load = findViewById(R.id.tx_highLoad);
        lbl_date = findViewById(R.id.tx_date);
        lbl_time = findViewById(R.id.tx_time);
        lbl_high_load = findViewById(R.id.tx_highLoad);

        btn_Connect.setText("اتصال به کنتور");
        btn_Connect.setBackgroundColor(Color.parseColor("#C62828"));
        btn_Connect.setFocusBackgroundColor(Color.parseColor("#F44336"));
        btn_Connect.setTextSize(17);
        btn_Connect.setRadius(10);
        btn_Connect.setIconPosition(FancyButton.POSITION_RIGHT);
        btn_Connect.setFontIconSize(30);

        progressDialog = new com.keec.caspian.Application.Dialogs.ProgressDialog(this);
        progressDialog.setCancelable(true);
    }

    public void FlagButtonsClick(View view) {
        switch (view.getId()) {
            case R.id.btn_Jam200:
                progressDialog.show();
                RequestMeterData(new _JAM_200(), JAM_200);
                break;
            case R.id.btn_Jam200t:
                progressDialog.show();
                RequestMeterData(new _JAM_200T(), JAM_200T);
                break;
            case R.id.btn_JAM300:
                progressDialog.show();
                RequestMeterData(new _JAM_300(), JAM_300);
                break;

            case R.id.btn_JAM300X:
                progressDialog.show();
                RequestMeterData(new _JAM_300_newVersion2Tavanir(), JAM_300_newVersion2Tavanir);
                break;
            case R.id.btn_HXE4:
                progressDialog.show();
                RequestMeterData(new _HEX_400(), HEX_400);
                break;

                case R.id.btn_ABB5_v5:
                progressDialog.show();
                RequestMeterData(new _ELSTER_A220(),ELSTER_A220 );
                break;

                case R.id.btn_ABB5_v7:
                progressDialog.show();
                RequestMeterData(new _ELSTER_A1350(), ELSTER_A1350);
                break;

        }
    }

    private void RequestMeterData(_Meter_Base meter, ConstDefines.MeterTypes meterType) {
        meter.initial();
        String command = meter.GetCommand_Rt1();
        socketClient.SendCommand(command.getBytes(), CommandType.ReadCounterData, meterType);
    }

    private void CheckWifi() {
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifi != null;

        if (wifi.isWifiEnabled()) {
            txt_wifi_name.setText(wifi.getConnectionInfo().getSSID().replace("\"", ""));
            img_wifi.setImageResource(R.drawable.wifi);
            btn_Connect.setEnabled(true);
        } else {
            txt_wifi_name.setText(getString(R.string.NotConnected));
            img_wifi.setImageResource(R.drawable.no_wifi);
            btn_Connect.setEnabled(false);
        }
    }

    private void BindControlsEvent() {
        btn_Connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btn_Connect.setVisibility(View.INVISIBLE);
                        lot_meter.setVisibility(View.VISIBLE);
                    }
                });
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        socketClient = new SocketClient(Setting.SERVER_IP, Setting.SERVER_PORT);
                    }
                }, 1000);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        progressDialog.hide();
        if (btn_Connect.getVisibility() == View.INVISIBLE) {
            btn_Connect.setVisibility(View.VISIBLE);
            lot_meter.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActionEvent(SocketConnectionLost event) {
        btn_Connect.setVisibility(View.VISIBLE);
        lot_meter.setVisibility(View.INVISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActionEvent(actionResult event) {
        if (event.isConnected()) {
            ly_wifi.setVisibility(View.GONE);
            ly_data.setVisibility(View.VISIBLE);
        } else {
            btn_Connect.setVisibility(View.VISIBLE);
            lot_meter.setVisibility(View.INVISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (event.getCommandType().equals("Connected")) {
            img_wifi.setImageResource(R.drawable.wifi);
            btn_Connect.setEnabled(true);
            lot_meter.setVisibility(View.INVISIBLE);
        }
        if (event.getCommandType().equals("NotConnected")) {
            img_wifi.setImageResource(R.drawable.no_wifi);
            btn_Connect.setEnabled(false);
            ly_data.setVisibility(View.GONE);
            ly_wifi.setVisibility(View.VISIBLE);
            btn_Connect.setVisibility(View.VISIBLE);
            lot_meter.setVisibility(View.INVISIBLE);
        }
        if (event.getCommandType().equals(CommandType.ReadCounterData)) {
            progressDialog.hide();
            allMeterReadData = event.getMessage().replaceAll("\0", "");
            ConstDefines.MeterTypes mt = event.getMeterType();
            parseReadData(allMeterReadData, "", mt);
        }
    }


    private void ShowData() {
        lbl_serial.setText(meterValuesStruct.GetSerial());
        lbl_low_load.setText(meterValuesStruct.getPowerlow());
        lbl_mid_load.setText(meterValuesStruct.getPowerMidd());
        lbl_high_load.setText(meterValuesStruct.getPowerhigh());
        lbl_reactive.setText(meterValuesStruct.GetRactive());
        lbl_maxi_meter.setText(meterValuesStruct.GetMaxiMeter());
        lbl_date.setText(meterValuesStruct.getdate());
        lbl_time.setText(meterValuesStruct.gettime());
    }

    public void parseReadData(String defaultData, String specialData, ConstDefines.MeterTypes inputMeterType) {

        meterValuesStruct.initial();
        meterSpecialValuesStruct.initial();
        parser.reset();
        ConstDefines.MeterTypes meterType = inputMeterType;
        if (meterType == UNDEFINED) {
            meterType = parser.getMeterTypeAndTipFromReadData(defaultData.substring(0, 100));
            if (meterType == UNDEFINED)
                return;
        }
        parser.setMeterType_(meterType);
        parser.setRawDataDefault(defaultData);
        parser.setRawDataSpecial(specialData);
        boolean parsedData = parser.parseMeter(meterValuesStruct, meterSpecialValuesStruct);
        String messageText;

        if (!parsedData || parser.isExceptionOccurred)
            messageText = (parser.occurredExceptionText.equals("")) ? "Data Not Parsed!" : parser.occurredExceptionText;
        else
            messageText = "Parsed Data:\n" + meterValuesStruct.getString() + "\n" + "Parsed Special Data:\n" + meterSpecialValuesStruct.getString();
        if (messageText.startsWith("Parsed"))
            ShowData();
    }
}
