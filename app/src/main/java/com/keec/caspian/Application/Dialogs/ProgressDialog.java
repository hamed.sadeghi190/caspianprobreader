package com.keec.caspian.Application.Dialogs;

import android.content.Context;
import android.os.Bundle;
import com.keec.caspian.R;

public class ProgressDialog extends android.app.ProgressDialog {

    public ProgressDialog(Context context) {
        super(context , R.style.ProgressDialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.progress_loading);
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }
}
