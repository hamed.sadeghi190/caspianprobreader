package com.keec.caspian.Application.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.keec.caspian.Constants.CommandType;
import com.keec.caspian.Constants.Commands;
import com.keec.caspian.R;
import com.keec.caspian.Utilities.Functions;
import com.keec.caspian.EventBusModels.MessageEvent;
import com.keec.caspian.Utilities.SocketClient;
import com.keec.caspian.EventBusModels.actionResult;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterSpecialValuesStruct;
import ParsingCommonFiles.Parsing.MeterValuesStruct;
import ParsingCommonFiles.Parsing.ParseReceivedData;
import androidx.appcompat.app.AppCompatActivity;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM200T_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.JAM_200T;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.UNDEFINED;

public class MainActivity extends AppCompatActivity {

    SocketClient socketClient;
    public static String SERVER_IP = "192.168.4.1";
    public static final int SERVER_PORT = 8888;
    private final ParseReceivedData parser = new ParseReceivedData();
    public static String allMeterReadData = "";

    MeterValuesStruct meterValuesStruct;
    MeterSpecialValuesStruct meterSpecialValuesStruct;

    ProgressDialog progressDialog;
    String counterName;
    private Button btn_Read;
    private LinearLayout ly_read;
    TextView lbl_serial, lbl_name,lbl_maximeter,lbl_ractive,lbl_lowload,lbl_midload,lbl_highload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        InitContorls();

        meterValuesStruct = new MeterValuesStruct();
        meterSpecialValuesStruct = new MeterSpecialValuesStruct();

        TextView txt_wifi_ssid = findViewById(R.id.txt_wifi_ssid);
        Button btn_Connect = findViewById(R.id.btn_Connect);
        Button btn_Read = findViewById(R.id.btn_Read);
        ly_read = findViewById(R.id.ly_read);

        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wifi != null;

        if (wifi.isWifiEnabled()) {
            txt_wifi_ssid.setText(wifi.getConnectionInfo().getSSID());
            btn_Connect.setEnabled(true);
        } else {
            txt_wifi_ssid.setText(getString(R.string.NotConnected));
            btn_Connect.setEnabled(false);
        }

        btn_Connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                socketClient = new SocketClient(SERVER_IP, SERVER_PORT);
            }
        });

        btn_Read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                byte[] CBR = Commands.CBR.getBytes();
                byte[] CounterName = Functions.hexStringToByteArray(Commands.CounterName);
                byte[] FullCommand = new byte[CBR.length + CounterName.length];
                System.arraycopy(CBR, 0, FullCommand, 0, CBR.length);
                System.arraycopy(CounterName, 0, FullCommand, CBR.length, CounterName.length);
                socketClient.SendCommand(FullCommand, CommandType.ReadCounterName, null);
            }
        });
    }

    private void InitContorls() {
        progressDialog = new com.keec.caspian.Application.Dialogs.ProgressDialog(this);
        progressDialog.setTitle("loading");
        progressDialog.setCancelable(true);

        lbl_serial = findViewById(R.id.lbl_serial);
        lbl_name = findViewById(R.id.lbl_name);
        lbl_maximeter = findViewById(R.id.lbl_maximeter);
        lbl_ractive = findViewById(R.id.lbl_ractive);
        lbl_lowload = findViewById(R.id.lbl_LowLop);
        lbl_midload = findViewById(R.id.lbl_midload);
        lbl_highload = findViewById(R.id.lbl_highload);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onActionEvent(actionResult event) {
        if (event.isConnected()) {
            ly_read.setVisibility(View.VISIBLE);
        } else {
            ly_read.setVisibility(View.INVISIBLE);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        String parsedData;
        if (event.getCommandType().equals(CommandType.ReadCounterData)) {
            progressDialog.hide();
            String data = event.getMessage();
            allMeterReadData = "";
            allMeterReadData = counterName.trim().concat(data);

            boolean isJam200 = isJam200Meter(allMeterReadData);
            parsedData = parseReadData(allMeterReadData, "", JAM_200T);


        }

        if (event.getCommandType().equals(CommandType.ReadCounterName)) {
            try {
                counterName = event.getMessage();
                Thread.sleep(500);
                socketClient.SendCommand(Functions.hexStringToByteArray(Commands.CounterData), CommandType.ReadCounterData, null);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    private void ShowData() {
        lbl_serial.setText(meterValuesStruct.GetSerial());
        lbl_lowload.setText(meterValuesStruct.getPowerlow());
        lbl_midload.setText(meterValuesStruct.getPowerMidd());
        lbl_highload.setText(meterValuesStruct.getPowerhigh());
        lbl_ractive.setText(meterValuesStruct.GetRactive());
        lbl_maximeter.setText(meterValuesStruct.GetMaxiMeter());
    }

    private boolean isJam200Meter(String allMeterReadData) {
        return allMeterReadData.contains(JAM200T_NAME);
    }

    public String parseReadData(String defaultData, String specialData, ConstDefines.MeterTypes inputMeterType) {

        meterValuesStruct.initial();
        meterSpecialValuesStruct.initial();
        parser.reset();
        ConstDefines.MeterTypes meterType = inputMeterType;
        if (meterType == UNDEFINED) {
            meterType = parser.getMeterTypeAndTipFromReadData(defaultData.substring(0, 100));
            if (meterType == UNDEFINED)
                return "\nError on detect meter type!\n";
        }
        parser.setMeterType_(meterType);
        parser.setRawDataDefault(defaultData);
        parser.setRawDataSpecial(specialData);
        boolean parsedData = parser.parseMeter(meterValuesStruct, meterSpecialValuesStruct);
        String messageText;

        if (!parsedData || parser.isExceptionOccurred)
            messageText = (parser.occurredExceptionText.equals("")) ? "Data Not Parsed!" : parser.occurredExceptionText;
        else
            messageText = "Parsed Data:\n" + meterValuesStruct.getString() + "\n" + "Parsed Special Data:\n" + meterSpecialValuesStruct.getString();
        if (messageText.startsWith("Parsed"))
            ShowData();
        return messageText;
    }

}
