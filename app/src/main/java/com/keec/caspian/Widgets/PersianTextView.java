package com.keec.caspian.Widgets;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.TextView;

import com.keec.caspian.Utilities.AppController;


@SuppressLint("AppCompatCustomView")
public class PersianTextView extends TextView {

    public PersianTextView(Context context) {
        super(context);
        if (!isInEditMode()) {
            setGravity(Gravity.RIGHT | Gravity.TOP);
            setTypeface(AppController.Iran);
        }
    }

    public PersianTextView(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        if (!isInEditMode()) {
            setTypeface(AppController.Iran);
        }
    }

    public PersianTextView(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        if (!isInEditMode()) {
            setTypeface(AppController.Iran);
        }
    }

}
