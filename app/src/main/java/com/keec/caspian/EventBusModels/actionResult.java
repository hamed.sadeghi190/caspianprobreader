package com.keec.caspian.EventBusModels;

public class actionResult {
    private boolean IsConnected;

    public boolean isConnected() {
        return IsConnected;
    }

    public void setConnected(boolean connected) {
        IsConnected = connected;
    }

    public actionResult(boolean isConnected) {
        IsConnected = isConnected;
    }
}

