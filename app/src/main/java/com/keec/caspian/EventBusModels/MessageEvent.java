package com.keec.caspian.EventBusModels;

import ParsingCommonFiles.Parsing.ConstDefines;

public class MessageEvent {
    private  String Message;
    private  String CommandType;
    private ConstDefines.MeterTypes MeterType;

    public ConstDefines.MeterTypes getMeterType() {
        return MeterType;
    }

    public void setMeterType(ConstDefines.MeterTypes meterType) {
        MeterType = meterType;
    }



    public String getCommandType() {
        return CommandType;
    }

    public void setCommandType(String commandType) {
        CommandType = commandType;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public MessageEvent(String message, String commandType, ConstDefines.MeterTypes meterType) {
        Message = message;
        CommandType = commandType;
        MeterType = meterType;
    }

}

