package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.KTC_NAME;



public class _KTC extends  _Meter_Base {

@Override
    public void initial(){
        this.setMeterName(KTC_NAME);

        this.setMeterSerialPrefix("0.0.0(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.setDateLength(9);
        this.setTimeLength(8);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerOutPrefixSize(0);
    }
}