package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterValuesStruct;

import static ParsingCommonFiles.Parsing.ConstDefines.RENAN_2000_NAME;



public class _RENAN_2000 extends  _Meter_Base {
    @Override
    public void initial(){
        this.setMeterName(RENAN_2000_NAME);
        this.setMeterNameAll("SLb5");

        this.setMeterSerialPrefix("C.1.0(");
        this.setMeterTimePrefix("");
        this.setMeterDatePrefix("");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(16);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(0);
        this.setMeterDatePrefixSize(0);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerOutPrefixSize(0);
    }
}