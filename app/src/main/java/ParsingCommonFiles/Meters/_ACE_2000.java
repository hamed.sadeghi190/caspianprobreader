package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterValuesStruct;

import static ParsingCommonFiles.Parsing.ConstDefines.ACE_2000_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.WATT_CONSTANT;



public class _ACE_2000 extends  _Meter_Base {
    @Override
    public void initial(){
        this.setMeterName(ACE_2000_NAME);

        this.setMeterSerialPrefix("C.1(");
        this.setMeterTimePrefix("1.0(");
        this.setMeterDatePrefix("1.0(");
        this.setMeterMaximumDemandPrefix("");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower1TotalPrefix("1.8(");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("");
        this.setMeterPowerOutPrefix("");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(7);
        this.setTimeLength(5);
        this.setDateLength(8);
        this.setMeterSerialPrefixSize(4);
        this.setPowersPlaceHolder(1);
        this.setMeterDateTimePrefixSize(4);
        this.setMeterTimePrefixSize(4 + 9);
        this.setMeterDatePrefixSize(4);
        this.setMeterMaximumDemandPrefixSize(0);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(0);
        this.setMeterCurrentPhasePrefixSize(0);
        this.setMeterPowerFactorPrefixSize(0);
        this.setMeterPowerOutPrefixSize(0);
    }
    @Override
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        setSerialStartIndex(rawData);
        setTimeStartIndex(rawData);
        setDateStartIndex(rawData);
        setPower1StartEndIndex(rawData, readType);
        setPower2StartEndIndex(rawData, readType);
        setPower3StartEndIndex(rawData, readType);
        setValuesToStruct(rawData, meterValuesStruct, readType);
    }
}