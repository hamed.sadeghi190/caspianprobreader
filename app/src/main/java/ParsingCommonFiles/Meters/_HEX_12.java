package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;



public class _HEX_12 extends  _Meter_Base {

    @Override
    public void initial(){

        this.setMeterSerialPrefix("C.1.0(");
        this.setMeterTimePrefix("1.4.0(");
        this.setMeterDatePrefix("1.4.0(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(8);
        this.setDateLength(8);
        this.setTimeLength(5);
        this.setTimePlaceHolder(9);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
    }
}