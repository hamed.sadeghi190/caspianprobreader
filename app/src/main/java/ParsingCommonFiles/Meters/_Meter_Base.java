package ParsingCommonFiles.Meters;

import java.util.ArrayList;

import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterSpecialValuesStruct;
import ParsingCommonFiles.Parsing.MeterValuesStruct;
import ParsingCommonFiles.StringTravers.DefaultValuesIndexes;
import ParsingCommonFiles.StringTravers.DefaultValuesPrefixes;
import ParsingCommonFiles.StringTravers.DefaultValuesPrefixesSize;
import ParsingCommonFiles.StringTravers.NamePrefixes;
import ParsingCommonFiles.StringTravers.SpecialValuesIndexes;
import ParsingCommonFiles.StringTravers.SpecialValuesPrefixes;
import ParsingCommonFiles.StringTravers.SpecialValuesPrefixesSize;

import static ParsingCommonFiles.Parsing.ConstDefines.K_WATT_CONSTANT;
import static ParsingCommonFiles.Parsing.ConstDefines.WATT_CONSTANT;


public class _Meter_Base {

    private ArrayList<String> RT1Command = new ArrayList<>();
    private DefaultValuesPrefixes defaultValuesPrefixes_ = new DefaultValuesPrefixes();
    private DefaultValuesPrefixesSize defaultValuesPrefixesSize_ = new DefaultValuesPrefixesSize();
    private DefaultValuesIndexes defaultValuesIndexes_ = new DefaultValuesIndexes();
    private NamePrefixes namePrefixes_ = new NamePrefixes();
    private SpecialValuesIndexes specialValuesIndexes_ = new SpecialValuesIndexes();
    private SpecialValuesPrefixes specialValuesPrefixes_ = new SpecialValuesPrefixes();
    private SpecialValuesPrefixesSize specialValuesPrefixesSize_ = new SpecialValuesPrefixesSize();
    private boolean applyOrderingCretria_ = true;
    private boolean checkSecondSerial_ = false;
    private boolean applyPower1OrderingCretria_ = false;

    public void initial() {
    }

    //Set values per meter type
    public void applyOrderingCretria(boolean apply) {
        applyOrderingCretria_ = apply;
    }

    public void checkForSecondSerialPrefix(boolean check) {
        checkSecondSerial_ = check;
    }

    public void applyPower1OrderingCretria(boolean apply) {
        applyPower1OrderingCretria_ = apply;
    }

    public void setMeterName(String meterName) {
        namePrefixes_.setMeterName(meterName);
    }

    public void setMeterName2(String meterName) {
        namePrefixes_.setMeterName2(meterName);
    }

    public void setMeterName3(String meterName) {
        namePrefixes_.setMeterName3(meterName);
    }

    public void setMeterName4(String meterName) {
        namePrefixes_.setMeterName4(meterName);
    }

    public void setMeterNameAll(String meterNameAll) {
        namePrefixes_.setMeterNameAll(meterNameAll);
    }

    public void setMeterSerialPrefix(String meterSerialPrefix) {
        defaultValuesPrefixes_.setMeterSerialPrefix(meterSerialPrefix);
    }

    public void setMeterSerialPrefix1(String meterSerialPrefix) {
        defaultValuesPrefixes_.setMeterSerialPrefix1(meterSerialPrefix);
    }

    public void setMeterTimePrefix(String meterTimePrefix) {
        defaultValuesPrefixes_.setMeterTimePrefix(meterTimePrefix);
    }

    public void setMeterDatePrefix(String meterDatePrefix) {
        defaultValuesPrefixes_.setMeterDatePrefix(meterDatePrefix);
    }

    public void setMeterMaximumDemandPrefix(String meterMaximumDemandPrefix) {
        defaultValuesPrefixes_.setMeterMaximumDemandPrefix(meterMaximumDemandPrefix);
    }

    public void setMeterPower1TotalPrefix(String meterPowerTotal) {
        defaultValuesPrefixes_.setMeterPower1TotalPrefix(meterPowerTotal);
    }

    public void setMeterPower1Prefix(String MeterPower1Prefix) {
        defaultValuesPrefixes_.setMeterPower1Prefix(MeterPower1Prefix);
    }

    public void setMeterPower2Prefix(String MeterPower2Prefix) {
        defaultValuesPrefixes_.setMeterPower2Prefix(MeterPower2Prefix);
    }

    public void setMeterPower3Prefix(String MeterPower3Prefix) {
        defaultValuesPrefixes_.setMeterPower3Prefix(MeterPower3Prefix);
    }

    public void setMeterPower4Prefix(String MeterPower4Prefix) {
        defaultValuesPrefixes_.setMeterPower4Prefix(MeterPower4Prefix);
    }

    public void setMeterPowerReActorPrefix(String meterPowerReActorPrefix) {
        defaultValuesPrefixes_.setMeterPowerReActorPrefix(meterPowerReActorPrefix);
    }

    public void setMeterPowerOutPrefix(String meterPowerOutPrefix) {
        defaultValuesPrefixes_.setMeterPowerOutPrefix(meterPowerOutPrefix);
    }

    public void setMeterVoltagePhase1Prefix(String voltagePhase1) {
        specialValuesPrefixes_.setMeterVoltagePhase1Prefix(voltagePhase1);
    }

    public void setMeterVoltagePhase2Prefix(String voltagePhase2) {
        specialValuesPrefixes_.setMeterVoltagePhase2Prefix(voltagePhase2);
    }

    public void setMeterVoltagePhase3Prefix(String voltagePhase3) {
        specialValuesPrefixes_.setMeterVoltagePhase3Prefix(voltagePhase3);
    }

    public void setMeterCurrentPhase1Prefix(String currentPhase1) {
        specialValuesPrefixes_.setMeterCurrentPhase1Prefix(currentPhase1);
    }

    public void setMeterCurrentPhase2Prefix(String currentPhase2) {
        specialValuesPrefixes_.setMeterCurrentPhase2Prefix(currentPhase2);
    }

    public void setMeterCurrentPhase3Prefix(String currentPhase3) {
        specialValuesPrefixes_.setMeterCurrentPhase3Prefix(currentPhase3);
    }

    public void setMeterPowerFactor1Prefix(String powerFactor1) {
        specialValuesPrefixes_.setMeterPowerFactor1Prefix(powerFactor1);
    }

    public void setMeterPowerFactor2Prefix(String powerFactor2) {
        specialValuesPrefixes_.setMeterPowerFactor2Prefix(powerFactor2);
    }

    public void setMeterPowerFactor3Prefix(String powerFactor3) {
        specialValuesPrefixes_.setMeterPowerFactor3Prefix(powerFactor3);
    }

    public void setMeterSerialPrefixSize(int meterSerialPrefixSize) {
        defaultValuesPrefixesSize_.setMeterSerialPrefixSize(meterSerialPrefixSize);
    }

    public void setMeterSerialPrefix1Size(int meterSerialPrefixSize) {
        defaultValuesPrefixesSize_.setMeterSerialPrefix1Size(meterSerialPrefixSize);
    }

    public void setMeterTimePrefixSize(int meterTimePrefixSize) {
        defaultValuesPrefixesSize_.setMeterTimePrefixSize(meterTimePrefixSize);
    }

    public void setMeterDatePrefixSize(int meterDatePrefixSize) {
        defaultValuesPrefixesSize_.setMeterDatePrefixSize(meterDatePrefixSize);
    }

    public void setMeterMaximumDemandPrefixSize(int meterMaximumDemandPrefixSize) {
        defaultValuesPrefixesSize_.setMeterMaximumDemandPrefixSize(meterMaximumDemandPrefixSize);
    }

    public void setMeterPower1PrefixSize(int power1PrefixSize) {
        defaultValuesPrefixesSize_.setMeterPower1PrefixSize(power1PrefixSize);
    }

    public void setMeterPower2PrefixSize(int power2PrefixSize) {
        defaultValuesPrefixesSize_.setMeterPower2PrefixSize(power2PrefixSize);
    }

    public void setMeterPower3PrefixSize(int power3PrefixSize) {
        defaultValuesPrefixesSize_.setMeterPower3PrefixSize(power3PrefixSize);
    }

    public void setMeterPower4PrefixSize(int power4PrefixSize) {
        defaultValuesPrefixesSize_.setMeterPower4PrefixSize(power4PrefixSize);
    }

    public void setMeterPowerReActorPrefixSize(int powerReActorPrefixSize) {
        defaultValuesPrefixesSize_.setMeterPowerReActorPrefixSize(powerReActorPrefixSize);
    }

    public void setMeterPowerOutPrefixSize(int powerOutPrefixSize) {
        defaultValuesPrefixesSize_.setMeterPowerOutPrefixSize(powerOutPrefixSize);
    }

    public void setMeterDateTimePrefixSize(int dateTimePrefixSize) {
        specialValuesPrefixesSize_.setMeterDateTimePrefixSize(dateTimePrefixSize);
    }

    public void setMeterVoltagePhasePrefixSize(int voltagePhasePrefixSize) {
        specialValuesPrefixesSize_.setMeterVoltagePhasePrefixSize(voltagePhasePrefixSize);
    }

    public void setMeterCurrentPhasePrefixSize(int currentPhasePrefixSize) {
        specialValuesPrefixesSize_.setMeterCurrentPhasePrefixSize(currentPhasePrefixSize);
    }

    public void setMeterPowerFactorPrefixSize(int powerFactorPrefixSize) {
        specialValuesPrefixesSize_.setMeterPowerFactorPrefixSize(powerFactorPrefixSize);
    }

    public void AddRt1CommandParameter(String param) {

        RT1Command.add(param);
    }

    public String GetCommand_Rt1() {
        StringBuilder Result = new StringBuilder();
        for (String param : RT1Command) {
            Result.append(param);
        }
        return Result.toString();
    }

    //Parsing Methods
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        setSerialStartIndex(rawData);
        setTimeStartIndex(rawData);
        setDateStartIndex(rawData);
        setMaximumDemandStartEndIndex(rawData);
        setPower1StartEndIndex(rawData, readType);
        setPower2StartEndIndex(rawData, readType);
        setPower3StartEndIndex(rawData, readType);
        setPower4StartEndIndex(rawData, readType);
        setPowerReactiveStartEndIndex(rawData);
        setPowerOutStartEndIndex(rawData);
        setValuesToStruct(rawData, meterValuesStruct, readType);
    }

    public void parse(String rawData, MeterSpecialValuesStruct meterSpecialValuesStruct) {
        rawData = rawData.replace("\r\n", "");
        rawData = rawData.replace(" ", "");
        setDateTimeStartEndIndex(rawData);
        setVoltagePhase1StartEndIndex(rawData);
        setVoltagePhase2StartEndIndex(rawData);
        setVoltagePhase3StartEndIndex(rawData);
        setCurrentPhase1StartEndIndex(rawData);
        setCurrentPhase2StartEndIndex(rawData);
        setCurrentPhase3StartEndIndex(rawData);
        setPowerFactor1StartEndIndex(rawData);
        setPowerFactor2StartEndIndex(rawData);
        setPowerFactor3StartEndIndex(rawData);
        setValuesToSpecialStruct(rawData, meterSpecialValuesStruct);
    }

    //Parsing Sub Methods Default Values
    void setSerialStartIndex(String rawData) {
        if (checkSecondSerial_ == false) {
            if (rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix()) > 0)
                defaultValuesIndexes_.setSerialStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix()) + defaultValuesPrefixesSize_.getMeterSerialPrefixSize());
            else
                defaultValuesIndexes_.setSerialStartIndex(0);
        } else {
            if (rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix()) > 0)
                defaultValuesIndexes_.setSerialStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix()) + defaultValuesPrefixesSize_.getMeterSerialPrefixSize());
            else if (rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix1()) > 0)
                defaultValuesIndexes_.setSerialStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterSerialPrefix1()) + defaultValuesPrefixesSize_.getMeterSerialPrefix1Size());
            else
                defaultValuesIndexes_.setSerialStartIndex(0);
        }
    }

    void setTimeStartIndex(String rawData) {
        if (rawData.indexOf(defaultValuesPrefixes_.getMeterTimePrefix()) > 0)
            defaultValuesIndexes_.setTimeStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterTimePrefix()) + defaultValuesPrefixesSize_.getMeterTimePrefixSize() + defaultValuesIndexes_.getTimePlaceHolder());
        else
            defaultValuesIndexes_.setTimeStartIndex(0);
    }

    void setDateStartIndex(String rawData) {
        if (rawData.indexOf(defaultValuesPrefixes_.getMeterDatePrefix()) > 0)
            defaultValuesIndexes_.setDateStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterDatePrefix()) + defaultValuesPrefixesSize_.getMeterDatePrefixSize());
        else
            defaultValuesIndexes_.setDateStartIndex(0);
    }

    void setSerialLength(int length) {
        defaultValuesIndexes_.setSerialLength(length);
    }

    void setTimePlaceHolder(int placeHolder) {
        defaultValuesIndexes_.setTimePlaceHolder(placeHolder);
    }

    void setTimeLength(int length) {
        defaultValuesIndexes_.setTimeLength(length);
    }

    void setTimeStartPoint(int startPoint) {
        defaultValuesIndexes_.setTimeStartPoint(startPoint);
    }

    void setDateLength(int length) {
        defaultValuesIndexes_.setDateLength(length);
    }

    void setDateReverse(Boolean isDateReverse) {
        defaultValuesIndexes_.setDateReverse(isDateReverse);
    }

    void setMaximumDemandStartEndIndex(String rawData) {
        defaultValuesIndexes_.setMaximumDemandStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterMaximumDemandPrefix()) + defaultValuesPrefixesSize_.getMeterMaximumDemandPrefixSize());
        defaultValuesIndexes_.setMaximumDemandEndIndex(rawData.indexOf(ConstDefines.KILO_WATT_CONSTANT, defaultValuesIndexes_.getMaximumDemandStartIndex()));
    }

    void setPowersPlaceHolder(int placeHolder) {
        defaultValuesIndexes_.setPowersPlaceHolder(placeHolder);
    }

    void setPower1StartEndIndex(String rawData, int readType) {
        if (readType == K_WATT_CONSTANT) {
            defaultValuesIndexes_.setPower1StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower1Prefix()) + defaultValuesPrefixesSize_.getMeterPower1PrefixSize());
            defaultValuesIndexes_.setPower1EndIndex(rawData.indexOf(ConstDefines.KILO_WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower1StartIndex()));
        } else if (readType == WATT_CONSTANT) {
            defaultValuesIndexes_.setPower1StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower1Prefix()) + defaultValuesPrefixesSize_.getMeterPower1PrefixSize());
            defaultValuesIndexes_.setPower1EndIndex(rawData.indexOf(ConstDefines.WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower1StartIndex()));
        }
    }

    void setPower2StartEndIndex(String rawData, int readType) {
        if (readType == K_WATT_CONSTANT) {
            defaultValuesIndexes_.setPower2StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower2Prefix()) + defaultValuesPrefixesSize_.getMeterPower2PrefixSize());
            defaultValuesIndexes_.setPower2EndIndex(rawData.indexOf(ConstDefines.KILO_WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower2StartIndex()));
        } else if (readType == WATT_CONSTANT) {
            defaultValuesIndexes_.setPower2StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower2Prefix()) + defaultValuesPrefixesSize_.getMeterPower2PrefixSize());
            defaultValuesIndexes_.setPower2EndIndex(rawData.indexOf(ConstDefines.WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower2StartIndex()));
        }
    }

    void setPower3StartEndIndex(String rawData, int readType) {
        if (readType == K_WATT_CONSTANT) {
            defaultValuesIndexes_.setPower3StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower3Prefix()) + defaultValuesPrefixesSize_.getMeterPower3PrefixSize());
            defaultValuesIndexes_.setPower3EndIndex(rawData.indexOf(ConstDefines.KILO_WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower3StartIndex()));
        } else if (readType == WATT_CONSTANT) {
            defaultValuesIndexes_.setPower3StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower3Prefix()) + defaultValuesPrefixesSize_.getMeterPower3PrefixSize());
            defaultValuesIndexes_.setPower3EndIndex(rawData.indexOf(ConstDefines.WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower3StartIndex()));
        }
    }

    void setPower4StartEndIndex(String rawData, int readType) {
        if (defaultValuesPrefixesSize_.getMeterPower4PrefixSize() > 0) {
            if (readType == K_WATT_CONSTANT) {
                defaultValuesIndexes_.setPower4StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower4Prefix()) + defaultValuesPrefixesSize_.getMeterPower4PrefixSize());
                defaultValuesIndexes_.setPower4EndIndex(rawData.indexOf(ConstDefines.KILO_WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower4StartIndex()));
            } else if (readType == WATT_CONSTANT) {
                defaultValuesIndexes_.setPower4StartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPower4Prefix()) + defaultValuesPrefixesSize_.getMeterPower4PrefixSize());
                defaultValuesIndexes_.setPower4EndIndex(rawData.indexOf(ConstDefines.WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPower4StartIndex()));
            }
        }
    }

    void setPowerReactiveStartEndIndex(String rawData) {
        defaultValuesIndexes_.setPowerReactorStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPowerReActorPrefix()) + defaultValuesPrefixesSize_.getMeterPowerReActorPrefixSize());
        defaultValuesIndexes_.setPowerReactorEndIndex(rawData.indexOf(ConstDefines.KILO_VAR_HOUR_CONSTANT, defaultValuesIndexes_.getPowerReactorStartIndex()));
    }

    void setPowerOutStartEndIndex(String rawData) {
        if (defaultValuesPrefixesSize_.getMeterPowerOutPrefixSize() > 0) {
            defaultValuesIndexes_.setPowerOutStartIndex(rawData.indexOf(defaultValuesPrefixes_.getMeterPowerOutPrefix()) + defaultValuesPrefixesSize_.getMeterPowerOutPrefixSize());
            defaultValuesIndexes_.setPowerOutEndIndex(rawData.indexOf(ConstDefines.KILO_WATT_HOUR_CONSTANT, defaultValuesIndexes_.getPowerOutStartIndex()));
        }
    }

    void setValuesToStruct(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        if (defaultValuesIndexes_.getSerialStartIndex() > 0) {
            if (defaultValuesIndexes_.getSerialLength() != ConstDefines.DEFAULT_INITIAL_VALUE) {
                if (checkCeiling(defaultValuesIndexes_.getSerialStartIndex(), defaultValuesIndexes_.getSerialLength(), rawData.length()))
                    meterValuesStruct.setMeterSerial(rawData.substring(defaultValuesIndexes_.getSerialStartIndex(), defaultValuesIndexes_.getSerialStartIndex() + defaultValuesIndexes_.getSerialLength()));
            } else {
                if (checkCeiling(defaultValuesIndexes_.getSerialStartIndex(), ConstDefines.METER_SERIAL_SIZE, rawData.length()))
                    meterValuesStruct.setMeterSerial(rawData.substring(defaultValuesIndexes_.getSerialStartIndex(), defaultValuesIndexes_.getSerialStartIndex() + ConstDefines.METER_SERIAL_SIZE));
            }
        }
        String cDate = getDateString(rawData);
        String cTime = getTimeString(rawData);
        meterValuesStruct.setMeterTime(prepareTimeValue(cTime));
        meterValuesStruct.setMeterDate(prepareDateValue(cDate, defaultValuesIndexes_.getDateReverse()));

        if (checkBond(defaultValuesIndexes_.getMaximumDemandStartIndex(), defaultValuesIndexes_.getMaximumDemandEndIndex()))
            meterValuesStruct.setMeterMaximumDimand(rawData.substring(defaultValuesIndexes_.getMaximumDemandStartIndex(), defaultValuesIndexes_.getMaximumDemandEndIndex()));

        preparePowersValues(rawData, meterValuesStruct, readType);
    }

    //Parsing Sub Methods Special Values
    protected void setDateTimeStartEndIndex(String rawData) {
        specialValuesIndexes_.setDateTimeStartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterDateTimePrefix())
                + specialValuesPrefixesSize_.getMeterDateTimePrefixSize());
        specialValuesIndexes_.setDateTimeEndIndex(rawData.indexOf(")", specialValuesIndexes_.getDateTimeStartIndex()));
    }

    protected void setVoltagePhase1StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterVoltagePhase1Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase1Prefix()) > 0) {
                specialValuesIndexes_.setVoltagePhase1StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase1Prefix())
                        + specialValuesPrefixesSize_.getMeterVoltagePhasePrefixSize());
                specialValuesIndexes_.setVoltagePhase1EndIndex(rawData.indexOf("*V)", specialValuesIndexes_.getVoltagePhase1StartIndex()));
            }
        }
    }

    protected void setVoltagePhase2StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterVoltagePhase2Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase2Prefix()) > 0) {
                specialValuesIndexes_.setVoltagePhase2StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase2Prefix())
                        + specialValuesPrefixesSize_.getMeterVoltagePhasePrefixSize());
                specialValuesIndexes_.setVoltagePhase2EndIndex(rawData.indexOf("*V)", specialValuesIndexes_.getVoltagePhase2StartIndex()));
            }
        }
    }

    protected void setVoltagePhase3StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterVoltagePhase3Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase3Prefix()) > 0) {
                specialValuesIndexes_.setVoltagePhase3StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterVoltagePhase3Prefix())
                        + specialValuesPrefixesSize_.getMeterVoltagePhasePrefixSize());
                specialValuesIndexes_.setVoltagePhase3EndIndex(rawData.indexOf("*V)", specialValuesIndexes_.getVoltagePhase3StartIndex()));
            }
        }
    }

    protected void setCurrentPhase1StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterCurrentPhase1Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase1Prefix()) > 0) {
                specialValuesIndexes_.setCurrentPhase1StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase1Prefix())
                        + specialValuesPrefixesSize_.getMeterCurrentPhasePrefixSize());
                specialValuesIndexes_.setCurrentPhase1EndIndex(rawData.indexOf("*A)", specialValuesIndexes_.getCurrentPhase1StartIndex()));
            }
        }
    }

    protected void setCurrentPhase2StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterCurrentPhase2Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase2Prefix()) > 0) {
                specialValuesIndexes_.setCurrentPhase2StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase2Prefix())
                        + specialValuesPrefixesSize_.getMeterCurrentPhasePrefixSize());
                specialValuesIndexes_.setCurrentPhase2EndIndex(rawData.indexOf("*A)", specialValuesIndexes_.getCurrentPhase2StartIndex()));
            }
        }
    }

    protected void setCurrentPhase3StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterCurrentPhase3Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase3Prefix()) > 0) {
                specialValuesIndexes_.setCurrentPhase3StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterCurrentPhase3Prefix())
                        + specialValuesPrefixesSize_.getMeterCurrentPhasePrefixSize());
                specialValuesIndexes_.setCurrentPhase3EndIndex(rawData.indexOf("*A)", specialValuesIndexes_.getCurrentPhase3StartIndex()));
            }
        }
    }

    protected void setPowerFactor1StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterPowerFactor1Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor1Prefix()) > 0) {
                specialValuesIndexes_.setPowerFactor1StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor1Prefix())
                        + specialValuesPrefixesSize_.getMeterPowerFactorPrefixSize());
                specialValuesIndexes_.setPowerFactor1EndIndex(rawData.indexOf(")", specialValuesIndexes_.getPowerFactor1StartIndex()));
            }
        }
    }

    protected void setPowerFactor2StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterPowerFactor2Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor2Prefix()) > 0) {
                specialValuesIndexes_.setPowerFactor2StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor2Prefix())
                        + specialValuesPrefixesSize_.getMeterPowerFactorPrefixSize());
                specialValuesIndexes_.setPowerFactor2EndIndex(rawData.indexOf(")", specialValuesIndexes_.getPowerFactor2StartIndex()));
            }
        }
    }

    protected void setPowerFactor3StartEndIndex(String rawData) {
        if (specialValuesPrefixes_.getMeterPowerFactor3Prefix() != "") {
            if (rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor3Prefix()) > 0) {
                specialValuesIndexes_.setPowerFactor3StartIndex(rawData.indexOf(specialValuesPrefixes_.getMeterPowerFactor3Prefix())
                        + specialValuesPrefixesSize_.getMeterPowerFactorPrefixSize());
                specialValuesIndexes_.setPowerFactor3EndIndex(rawData.indexOf(")", specialValuesIndexes_.getPowerFactor3StartIndex()));
            }
        }
    }

    protected void setValuesToSpecialStruct(String rawData, MeterSpecialValuesStruct meterSpecialValuesStruct) {
        meterSpecialValuesStruct.setDateTime(ConstDefines.DASH_CONSTANT);
        if (checkBond(specialValuesIndexes_.getVoltagePhase1StartIndex(), specialValuesIndexes_.getVoltagePhase1EndIndex()))
            meterSpecialValuesStruct.setPhaseVoltage1(rawData.substring(specialValuesIndexes_.getVoltagePhase1StartIndex(), specialValuesIndexes_.getVoltagePhase1EndIndex()));

        if (checkBond(specialValuesIndexes_.getVoltagePhase2StartIndex(), specialValuesIndexes_.getVoltagePhase2EndIndex()))
            meterSpecialValuesStruct.setPhaseVoltage2(rawData.substring(specialValuesIndexes_.getVoltagePhase2StartIndex(), specialValuesIndexes_.getVoltagePhase2EndIndex()));

        if (checkBond(specialValuesIndexes_.getVoltagePhase3StartIndex(), specialValuesIndexes_.getVoltagePhase3EndIndex()))
            meterSpecialValuesStruct.setPhaseVoltage3(rawData.substring(specialValuesIndexes_.getVoltagePhase3StartIndex(), specialValuesIndexes_.getVoltagePhase3EndIndex()));

        if (checkBond(specialValuesIndexes_.getCurrentPhase1StartIndex(), specialValuesIndexes_.getCurrentPhase1EndIndex()))
            meterSpecialValuesStruct.setPhaseCurrent1(rawData.substring(specialValuesIndexes_.getCurrentPhase1StartIndex(), specialValuesIndexes_.getCurrentPhase1EndIndex()));

        if (checkBond(specialValuesIndexes_.getCurrentPhase2StartIndex(), specialValuesIndexes_.getCurrentPhase2EndIndex()))
            meterSpecialValuesStruct.setPhaseCurrent2(rawData.substring(specialValuesIndexes_.getCurrentPhase2StartIndex(), specialValuesIndexes_.getCurrentPhase2EndIndex()));

        if (checkBond(specialValuesIndexes_.getCurrentPhase3StartIndex(), specialValuesIndexes_.getCurrentPhase3EndIndex()))
            meterSpecialValuesStruct.setPhaseCurrent3(rawData.substring(specialValuesIndexes_.getCurrentPhase3StartIndex(), specialValuesIndexes_.getCurrentPhase3EndIndex()));

        if (checkBond(specialValuesIndexes_.getPowerFactor1StartIndex(), specialValuesIndexes_.getPowerFactor1EndIndex()))
            meterSpecialValuesStruct.setPowerFactor1(rawData.substring(specialValuesIndexes_.getPowerFactor1StartIndex(), specialValuesIndexes_.getPowerFactor1EndIndex()));

        if (checkBond(specialValuesIndexes_.getPowerFactor2StartIndex(), specialValuesIndexes_.getPowerFactor2EndIndex()))
            meterSpecialValuesStruct.setPowerFactor2(rawData.substring(specialValuesIndexes_.getPowerFactor2StartIndex(), specialValuesIndexes_.getPowerFactor2EndIndex()));

        if (checkBond(specialValuesIndexes_.getPowerFactor3StartIndex(), specialValuesIndexes_.getPowerFactor3EndIndex()))
            meterSpecialValuesStruct.setPowerFactor3(rawData.substring(specialValuesIndexes_.getPowerFactor3StartIndex(), specialValuesIndexes_.getPowerFactor3EndIndex()));
    }

    private boolean checkBond(int startIndex, int endIndex) {
        return (startIndex > 1 && endIndex > 1 && endIndex > startIndex) ? true : false;
    }

    private boolean checkCeiling(int startIndex, int endIndex, int length) {
        return (startIndex + endIndex <= length) ? true : false;
    }

    private String getDateString(String rawData) {
        if (defaultValuesIndexes_.getDateStartIndex() > 1) {
            if (defaultValuesIndexes_.getDateLength() != ConstDefines.DEFAULT_INITIAL_VALUE) {
                if (checkBond(defaultValuesIndexes_.getDateStartIndex(), defaultValuesIndexes_.getDateStartIndex() + defaultValuesIndexes_.getDateLength()))
                    if (checkCeiling(defaultValuesIndexes_.getDateStartIndex(), defaultValuesIndexes_.getDateLength(), rawData.length()))
                        return rawData.substring(defaultValuesIndexes_.getDateStartIndex(), defaultValuesIndexes_.getDateStartIndex() + defaultValuesIndexes_.getDateLength());
            } else {
                if (checkBond(defaultValuesIndexes_.getDateStartIndex(), defaultValuesIndexes_.getDateStartIndex() + ConstDefines.METER_DATE_SIZE))
                    if (checkCeiling(defaultValuesIndexes_.getDateStartIndex(), ConstDefines.METER_DATE_SIZE, rawData.length()))
                        return rawData.substring(defaultValuesIndexes_.getDateStartIndex(), defaultValuesIndexes_.getDateStartIndex() + ConstDefines.METER_DATE_SIZE);
            }
        }
        return ConstDefines.DASH_CONSTANT;
    }

    private String getTimeString(String rawData) {
        if (defaultValuesIndexes_.getTimeStartIndex() > 1) {
            if (defaultValuesIndexes_.getTimeLength() != ConstDefines.DEFAULT_INITIAL_VALUE)
                if (checkBond(defaultValuesIndexes_.getTimeStartIndex() + defaultValuesIndexes_.getTimeStartPoint(), defaultValuesIndexes_.getTimeStartIndex() + defaultValuesIndexes_.getTimeLength()))
                    if (checkCeiling(defaultValuesIndexes_.getTimeStartIndex() + defaultValuesIndexes_.getTimeStartPoint(), defaultValuesIndexes_.getTimeLength(), rawData.length()))
                        return rawData.substring(defaultValuesIndexes_.getTimeStartIndex() + defaultValuesIndexes_.getTimeStartPoint(), defaultValuesIndexes_.getTimeStartIndex() + defaultValuesIndexes_.getTimeLength());
                    else if (checkBond(defaultValuesIndexes_.getTimeStartIndex(), defaultValuesIndexes_.getTimeStartIndex() + ConstDefines.METER_TIME_SIZE))
                        if (checkCeiling(defaultValuesIndexes_.getTimeStartIndex(), ConstDefines.METER_TIME_SIZE, rawData.length()))
                            return rawData.substring(defaultValuesIndexes_.getTimeStartIndex(), defaultValuesIndexes_.getTimeStartIndex() + ConstDefines.METER_TIME_SIZE);
        }
        return ConstDefines.DASH_CONSTANT;
    }

    private String prepareTimeValue(String inputTime) {
        if (inputTime == ConstDefines.DASH_CONSTANT)
            return inputTime;
        String retVal;
        if (inputTime.indexOf(':') > 0) {
            retVal = inputTime.substring(0, 2) + ":" + inputTime.substring(3, 5);
            if (inputTime.length() > 5)
                retVal += ":" + inputTime.substring(6, 8);
        } else {
            if (inputTime.length() == 4)
                retVal = inputTime.substring(0, 2) + ":" + inputTime.substring(2, 4);
            else
                retVal = inputTime.substring(0, 2) + ":" + inputTime.substring(2, 4) + ":" + inputTime.substring(4, 6);
        }
        return retVal;
    }

    private String prepareDateValue(String inputDate, boolean isReverse) {
        if (inputDate == ConstDefines.DASH_CONSTANT)
            return inputDate;
        String retVal;
        if (inputDate.indexOf('-') > 0 || inputDate.indexOf('.') > 0 || inputDate.indexOf('_') > 0) {
            if (inputDate.length() >= 10)
                retVal = inputDate.substring(2, 4) + "." + inputDate.substring(5, 7) + "." + inputDate.substring(8, 10);
            else
                retVal = inputDate.substring(0, 2) + "." + inputDate.substring(3, 5) + "." + inputDate.substring(6, 8);
        } else {
            if (inputDate.length() == 6) {
                if (!isReverse)
                    retVal = inputDate.substring(0, 2) + "." + inputDate.substring(2, 4) + "." + inputDate.substring(4, 6);
                else {
                    String year = inputDate.substring(4, 6);
                    String month = inputDate.substring(2, 4);
                    String day = inputDate.substring(0, 2);
                    retVal = day + "." + month + "." + year;
                }
            } else {
                if (!isReverse)
                    retVal = inputDate.substring(0, 2) + "." + inputDate.substring(2, 4) + "." + inputDate.substring(4, 8);
                else {
                    String year = inputDate.substring(0, 4);
                    String month = inputDate.substring(4, 6);
                    String day = inputDate.substring(6, 8);
                    retVal = day + "." + month + "." + year;
                }

            }

        }
        return retVal;
    }

    private void preparePowersValues(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        if (applyOrderingCretria_) {
            try {
                meterValuesStruct.setMeterPowerActiveT1(ConstDefines.DASH_CONSTANT);
                if (applyPower1OrderingCretria_ || defaultValuesIndexes_.getPower1StartIndex() > defaultValuesIndexes_.getMaximumDemandStartIndex())
                    if (checkBond(defaultValuesIndexes_.getPower1StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower1EndIndex()))
                        meterValuesStruct.setMeterPowerActiveT1(rawData.substring(defaultValuesIndexes_.getPower1StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower1EndIndex()));

                meterValuesStruct.setMeterPowerActiveT2(ConstDefines.DASH_CONSTANT);
                if (defaultValuesIndexes_.getPower2StartIndex() > defaultValuesIndexes_.getPower1StartIndex())
                    if (checkBond(defaultValuesIndexes_.getPower2StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower2EndIndex()))
                        meterValuesStruct.setMeterPowerActiveT2(rawData.substring(defaultValuesIndexes_.getPower2StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower2EndIndex()));

                meterValuesStruct.setMeterPowerActiveT3(ConstDefines.DASH_CONSTANT);
                if (defaultValuesIndexes_.getPower3StartIndex() > defaultValuesIndexes_.getPower2StartIndex())
                    if (checkBond(defaultValuesIndexes_.getPower3StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower3EndIndex()))
                        meterValuesStruct.setMeterPowerActiveT3(rawData.substring(defaultValuesIndexes_.getPower3StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower3EndIndex()));

                meterValuesStruct.setMeterPowerActiveT4(ConstDefines.DASH_CONSTANT);
                if (defaultValuesIndexes_.getPower4StartIndex() > defaultValuesIndexes_.getPower3StartIndex())
                    if (checkBond(defaultValuesIndexes_.getPower4StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower4EndIndex()))
                        meterValuesStruct.setMeterPowerActiveT4(rawData.substring(defaultValuesIndexes_.getPower4StartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPower4EndIndex()));

                meterValuesStruct.setMeterPowerReActive(ConstDefines.DASH_CONSTANT);
                if (defaultValuesIndexes_.getPowerReactorStartIndex() > defaultValuesIndexes_.getPower4StartIndex())
                    if (checkBond(defaultValuesIndexes_.getPowerReactorStartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPowerReactorEndIndex()))
                        meterValuesStruct.setMeterPowerReActive(rawData.substring(defaultValuesIndexes_.getPowerReactorStartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPowerReactorEndIndex()));

                meterValuesStruct.setMeterPowerOut(ConstDefines.DASH_CONSTANT);
                if (defaultValuesPrefixesSize_.getMeterPowerOutPrefixSize() > 0)
                    if (checkBond(defaultValuesIndexes_.getPowerOutStartIndex(), defaultValuesIndexes_.getPowerOutEndIndex()))
                        meterValuesStruct.setMeterPowerOut(rawData.substring(defaultValuesIndexes_.getPowerOutStartIndex(), defaultValuesIndexes_.getPowerOutEndIndex()));

                try {
                    meterValuesStruct.setMeterPowerReActive(rawData.substring(defaultValuesIndexes_.getPowerReactorStartIndex() + defaultValuesIndexes_.getPowersPlaceHolder(), defaultValuesIndexes_.getPowerReactorEndIndex()));
                } catch (Exception e) {
                    meterValuesStruct.setMeterPowerReActive(ConstDefines.DASH_CONSTANT);
                }
            } catch (Exception exp) {
                System.err.print("Exp");
            }
        } else {
            if (checkBond(defaultValuesIndexes_.getPower1StartIndex(), defaultValuesIndexes_.getPower1EndIndex()))
                meterValuesStruct.setMeterPowerActiveT1(rawData.substring(defaultValuesIndexes_.getPower1StartIndex(), defaultValuesIndexes_.getPower1EndIndex()));
            if (checkBond(defaultValuesIndexes_.getPower2StartIndex(), defaultValuesIndexes_.getPower2EndIndex()))
                meterValuesStruct.setMeterPowerActiveT2(rawData.substring(defaultValuesIndexes_.getPower2StartIndex(), defaultValuesIndexes_.getPower2EndIndex()));
            if (checkBond(defaultValuesIndexes_.getPower3StartIndex(), defaultValuesIndexes_.getPower3EndIndex()))
                meterValuesStruct.setMeterPowerActiveT3(rawData.substring(defaultValuesIndexes_.getPower3StartIndex(), defaultValuesIndexes_.getPower3EndIndex()));
            if (checkBond(defaultValuesIndexes_.getPowerReactorStartIndex(), defaultValuesIndexes_.getPowerReactorEndIndex()))
                meterValuesStruct.setMeterPowerReActive(rawData.substring(defaultValuesIndexes_.getPowerReactorStartIndex(), defaultValuesIndexes_.getPowerReactorEndIndex()));

            if (defaultValuesIndexes_.getPower4StartIndex() > 1)
                meterValuesStruct.setMeterPowerActiveT4(rawData.substring(defaultValuesIndexes_.getPower4StartIndex(), defaultValuesIndexes_.getPower4EndIndex()));

            if (defaultValuesIndexes_.getPowerOutStartIndex() > 1)
                meterValuesStruct.setMeterPowerOut(rawData.substring(defaultValuesIndexes_.getPowerOutStartIndex(), defaultValuesIndexes_.getPowerOutEndIndex()));
            try {
                meterValuesStruct.setMeterPowerReActive(rawData.substring(defaultValuesIndexes_.getPowerReactorStartIndex(), defaultValuesIndexes_.getPowerReactorEndIndex()));
            } catch (Exception e) {
                meterValuesStruct.setMeterPowerReActive(ConstDefines.DASH_CONSTANT);
            }
        }
        if (readType == WATT_CONSTANT) {
            meterValuesStruct.setMeterPowerActiveT1(String.valueOf(meterValuesStruct.getPowerActiveT1() / 1000));
            meterValuesStruct.setMeterPowerActiveT2(String.valueOf(meterValuesStruct.getPowerActiveT2() / 1000));
            meterValuesStruct.setMeterPowerActiveT3(String.valueOf(meterValuesStruct.getPowerActiveT3() / 1000));
            meterValuesStruct.setMeterPowerActiveT4(String.valueOf(meterValuesStruct.getPowerActiveT4() / 1000));
        }
    }

    public void resetStructuresFields() {
        defaultValuesIndexes_.resetAllFields();
        defaultValuesPrefixes_.resetAllFields();
        defaultValuesPrefixesSize_.resetAllFields();
        namePrefixes_.resetAllFields();
        specialValuesIndexes_.resetAllFields();
        specialValuesPrefixes_.resetAllFields();
        specialValuesPrefixesSize_.resetAllFields();
        applyOrderingCretria_ = true;
        applyPower1OrderingCretria_ = false;
    }

    public void testParse(String dd, MeterValuesStruct mvs, int readType) {
        setSerialStartIndex(dd);
        setTimeStartIndex(dd);
        setDateStartIndex(dd);
        setMaximumDemandStartEndIndex(dd);
        setPower1StartEndIndex(dd, readType);
        setPower2StartEndIndex(dd, readType);
        setPower3StartEndIndex(dd, readType);
        setPower4StartEndIndex(dd, readType);
        setPowerReactiveStartEndIndex(dd);
        setPowerOutStartEndIndex(dd);
        setValuesToStruct(dd, mvs, readType);
    }
}

