package ParsingCommonFiles.Meters;

import static ParsingCommonFiles.Parsing.ConstDefines.SNH_NAME;



public class _SNH extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(SNH_NAME);

        this.setMeterSerialPrefix("1-0:0.0.0.255(   ");
        this.setMeterTimePrefix("1-0:0.9.1.255(");
        this.setMeterDatePrefix("1-0:0.9.2.255(");
        this.setMeterMaximumDemandPrefix("1-0:1.6.0.65(");
        this.setMeterPower1TotalPrefix("1-0:1.8.0.65(");
        this.setMeterPower1Prefix("1-0:1.8.1.65(");
        this.setMeterPower2Prefix("1-0:1.8.2.65(");
        this.setMeterPower3Prefix("1-0:1.8.3.65(");
        this.setMeterPower4Prefix("1-0:1.8.4.65(");
        this.setMeterPowerReActorPrefix("1-0:3.8.0.65(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("1-0:32.7.0.255(");
        this.setMeterVoltagePhase2Prefix("1-0:52.7.0.255(");
        this.setMeterVoltagePhase3Prefix("1-0:72.7.0.255(");
        this.setMeterCurrentPhase1Prefix("1-0:31.7.0.255(");
        this.setMeterCurrentPhase2Prefix("1-0:51.7.0.255(");
        this.setMeterCurrentPhase3Prefix("1-0:71.7.0.255(");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(13);
        this.setMeterSerialPrefixSize(17);
        this.setMeterTimePrefixSize(14);
        this.setMeterDatePrefixSize(14);
        this.setDateLength(10);
        this.setTimeLength(8);
        this.setMeterMaximumDemandPrefixSize(13);
        this.setMeterPower1PrefixSize(13);
        this.setMeterPower2PrefixSize(13);
        this.setMeterPower3PrefixSize(13);
        this.setMeterPower4PrefixSize(13);
        this.setMeterPowerReActorPrefixSize(13);
        this.setMeterVoltagePhasePrefixSize(15);
        this.setMeterCurrentPhasePrefixSize(15);
        this.setMeterPowerOutPrefixSize(0);
    }

}