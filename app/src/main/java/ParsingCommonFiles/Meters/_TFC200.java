package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import static ParsingCommonFiles.Parsing.ConstDefines.TFC_NAME;



public class _TFC200 extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(TFC_NAME);

        this.setMeterSerialPrefix("0.0.1(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("");
        this.setMeterVoltagePhase2Prefix("");
        this.setMeterVoltagePhase3Prefix("");
        this.setMeterCurrentPhase1Prefix("");
        this.setMeterCurrentPhase2Prefix("");
        this.setMeterCurrentPhase3Prefix("");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(6);
        this.setDateLength(8);
        this.setTimeLength(4);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(0);
        this.setMeterCurrentPhasePrefixSize(0);
        this.setMeterPowerOutPrefixSize(0);
    }

}