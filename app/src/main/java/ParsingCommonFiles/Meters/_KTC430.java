package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import static ParsingCommonFiles.Parsing.ConstDefines.KTC430_NAME;



public class _KTC430 extends  _Meter_Base {

@Override
    public void initial(){
        this.setMeterName(KTC430_NAME);

        this.setMeterSerialPrefix("0.0.0(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterMaximumDemandPrefix("");
        this.setMeterPower1TotalPrefix("");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("");
        this.setMeterPowerOutPrefix("");

        this.setDateLength(8);
        this.setTimeLength(8);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(0);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(0);
        this.setMeterPowerOutPrefixSize(0);
    }
}