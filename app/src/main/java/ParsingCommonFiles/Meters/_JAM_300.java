package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.JAM_300_NAME;



public class _JAM_300 extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(JAM_300_NAME);
        this.setMeterNameAll("JAM300");

        this.setMeterSerialPrefix("0.0.0(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("32.7(");
        this.setMeterVoltagePhase2Prefix("52.7(");
        this.setMeterVoltagePhase3Prefix("72.7(");
        this.setMeterCurrentPhase1Prefix("31.7(");
        this.setMeterCurrentPhase2Prefix("51.7(");
        this.setMeterCurrentPhase3Prefix("71.7(");
        this.setMeterPowerFactor1Prefix("");
        this.setMeterPowerFactor2Prefix("");
        this.setMeterPowerFactor3Prefix("");

        this.applyPower1OrderingCretria(true);
        this.setTimeLength(8);
        this.setDateLength(8);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);;
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerOutPrefixSize(0);

        this.setMeterDateTimePrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(5);
        this.setMeterCurrentPhasePrefixSize(5);
        CreateRt1Command();
    }

    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:0.0.0;");
        this.AddRt1CommandParameter("P02:0.9.1;");
        this.AddRt1CommandParameter("P03:0.9.2;");
        this.AddRt1CommandParameter("P04:1.8.1;");
        this.AddRt1CommandParameter("P05:1.8.2;");
        this.AddRt1CommandParameter("P06:1.8.3;");
        this.AddRt1CommandParameter("P07:1.8.0;");
        this.AddRt1CommandParameter("P08:3.8.0;");
        this.AddRt1CommandParameter("P09:31.7;");
        this.AddRt1CommandParameter("P10:51.7;");
        this.AddRt1CommandParameter("P11:71.7;");
        this.AddRt1CommandParameter("P12:32.7;");
        this.AddRt1CommandParameter("P13:52.7;");
        this.AddRt1CommandParameter("P14:72.7;");

    }
}
