package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A220_NAME;


public class _ELSTER_A220 extends _Meter_Base {
    @Override
    public void initial() {
        this.setMeterName(ELSTER_A220_NAME);
        this.setMeterNameAll("/ABB5\\@V7");

        this.setMeterSerialPrefix("C.1.0(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.1(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("32.7.0(");
        this.setMeterVoltagePhase2Prefix("");
        this.setMeterVoltagePhase3Prefix("");
        this.setMeterCurrentPhase1Prefix("31.7.0(");
        this.setMeterCurrentPhase2Prefix("");
        this.setMeterCurrentPhase3Prefix("");
        this.setMeterPowerFactor1Prefix("33.7.0(");
        this.setMeterPowerFactor2Prefix("");
        this.setMeterPowerFactor3Prefix("");

        this.setDateLength(6);
        this.setTimeLength(6);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);

        this.setMeterDateTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterVoltagePhasePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        this.setMeterCurrentPhasePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        this.setMeterPowerFactorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);

        this.setMeterPowerOutPrefixSize(0);
        CreateRt1Command();
    }
    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:1.8.0;");
        this.AddRt1CommandParameter("P02:1.8.1;");
        this.AddRt1CommandParameter("P03:1.8.2;");
        this.AddRt1CommandParameter("P04:1.8.3;");
        this.AddRt1CommandParameter("P05:3.8.0;");
    }
}
