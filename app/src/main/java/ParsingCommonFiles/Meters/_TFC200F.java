package ParsingCommonFiles.Meters;

import static ParsingCommonFiles.Parsing.ConstDefines.TFC_NAME;



public class _TFC200F extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(TFC_NAME);

        this.setMeterSerialPrefix("00.00.01(");
        this.setMeterTimePrefix("00.09.01(");
        this.setMeterDatePrefix("00.09.02(");
        this.setMeterMaximumDemandPrefix("01.06.00(");
        this.setMeterPower1TotalPrefix("01.08.00(");
        this.setMeterPower1Prefix("01.08.01(");
        this.setMeterPower2Prefix("01.08.02(");
        this.setMeterPower3Prefix("01.08.03(");
        this.setMeterPower4Prefix("01.08.04(");
        this.setMeterPowerReActorPrefix("");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("32.07.00(");
        this.setMeterCurrentPhase1Prefix("31.07.00(");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(8);
        this.setDateLength(8);
        this.setTimeLength(6);
        this.setDateReverse(true);
        this.setMeterSerialPrefixSize(9);
        this.setMeterTimePrefixSize(9);
        this.setMeterDatePrefixSize(9);
        this.setMeterMaximumDemandPrefixSize(9);
        this.setMeterPower1PrefixSize(9);
        this.setMeterPower2PrefixSize(9);
        this.setMeterPower3PrefixSize(9);
        this.setMeterPower4PrefixSize(9);
        this.setMeterPowerReActorPrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(9);
        this.setMeterCurrentPhasePrefixSize(9);
        this.setMeterPowerOutPrefixSize(0);
    }

}