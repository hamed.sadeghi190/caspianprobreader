package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterValuesStruct;



public class _ISKRA extends  _Meter_Base {
    private String serialPrefix = "00(";
    private String timePrefix = "51(";
    private String datePrefix = "52(";
    private String maximumDemandPrefix = "11(";
    private String maximumDemand1Prefix = "21(";
    private String maximumDemand2Prefix = "31(";
    private String power1Prefix = "10(";
    private String power2Prefix = "20(";
    private String power3Prefix = "30(";
    private String powerReactivePrefix = "80(";
    private String switchOverPrefix = "940(";

    private int prefixSize = 3;
    private int switchOverSize = 4;

    @Override
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        int serialStartIndex = rawData.indexOf(serialPrefix) + prefixSize;
        int timeStartIndex = rawData.indexOf(timePrefix) + prefixSize;
        int dateStartIndex = rawData.indexOf(datePrefix) + prefixSize;
        int maximumDemand1StartIndex = rawData.indexOf(maximumDemandPrefix) + prefixSize;
        int cnt = 0;
        //////////////////////////START MAXIMUM DEMAND////////////////////////////////
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt == 0)
                    io = Integer.parseInt(rawData.substring(maximumDemand1StartIndex-4, maximumDemand1StartIndex-3));
                else
                    io = Integer.parseInt(rawData.substring(maximumDemand1StartIndex-1, maximumDemand1StartIndex));

                maximumDemand1StartIndex = rawData.indexOf(maximumDemandPrefix,maximumDemand1StartIndex + prefixSize) ;
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    maximumDemand1StartIndex += prefixSize;
                break;
            }
        }

        cnt = 0;
        int maximumDemand2StartIndex = rawData.indexOf(maximumDemand1Prefix) + prefixSize;
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    Integer.parseInt(rawData.substring(maximumDemand2StartIndex -4, maximumDemand2StartIndex-prefixSize));
                else
                    Integer.parseInt(rawData.substring(maximumDemand2StartIndex-1, maximumDemand2StartIndex));
                maximumDemand2StartIndex = rawData.indexOf(maximumDemand1Prefix,maximumDemand2StartIndex +prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    maximumDemand2StartIndex += prefixSize;
                break;
            }
        }
        cnt = 0;
        int maximumDemand3StartIndex = rawData.indexOf(maximumDemand2Prefix) + prefixSize;

        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    io = Integer.parseInt(rawData.substring(maximumDemand3StartIndex -4, maximumDemand3StartIndex - prefixSize));
                else
                    io = Integer.parseInt(rawData.substring(maximumDemand3StartIndex -1, maximumDemand3StartIndex ));
                maximumDemand3StartIndex = rawData.indexOf(maximumDemand2Prefix,maximumDemand3StartIndex + prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    maximumDemand3StartIndex  += prefixSize;
                break;
            }
        }
        /////////////////////////////////////END MAXIMUM DEMAND////////////////////////////////
        //////////////////////////START POWER ACTIVES ////////////////////////////////
        cnt = 0;
        int power1StartIndex = rawData.indexOf(power1Prefix) + prefixSize;
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    io = Integer.parseInt(rawData.substring(power1StartIndex-4, power1StartIndex-prefixSize));
                else
                    io = Integer.parseInt(rawData.substring(power1StartIndex-1, power1StartIndex));
                power1StartIndex = rawData.indexOf(power1Prefix,power1StartIndex + prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    power1StartIndex += prefixSize;
                break;
            }
        }

        int power2StartIndex = rawData.indexOf(power2Prefix) + prefixSize;
        cnt = 0;
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    io = Integer.parseInt(rawData.substring(power2StartIndex-4, power2StartIndex-prefixSize));
                else
                    io = Integer.parseInt(rawData.substring(power2StartIndex-1, power2StartIndex));
                power2StartIndex = rawData.indexOf(power2Prefix,power2StartIndex +prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    power2StartIndex += prefixSize;
                break;
            }
        }

        int power3StartIndex = rawData.indexOf(power3Prefix) + prefixSize;
        cnt = 0;
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    io = Integer.parseInt(rawData.substring(power3StartIndex-4, power3StartIndex-prefixSize));
                else
                    io = Integer.parseInt(rawData.substring(power3StartIndex-1, power3StartIndex));
                power3StartIndex = rawData.indexOf(power3Prefix,power3StartIndex +prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    power3StartIndex += prefixSize;
                break;
            }
        }
        //////////////////////////END POWER ACTIVES ////////////////////////////////
        //////////////////////////START REACTIVES ////////////////////////////////
        int powerReactiveStartIndex = rawData.indexOf(powerReactivePrefix)	+ prefixSize;
        cnt = 0;
        while(true)
        {
            try
            {
                int io = 0;
                if(cnt== 0)
                    io = Integer.parseInt(rawData.substring(powerReactiveStartIndex-4, powerReactiveStartIndex-prefixSize));
                else
                    io = Integer.parseInt(rawData.substring(powerReactiveStartIndex-1, powerReactiveStartIndex));
                powerReactiveStartIndex = rawData.indexOf(powerReactivePrefix,powerReactiveStartIndex + prefixSize);
                cnt++;
            }
            catch(Exception e)
            {
                if(cnt>0)
                    powerReactiveStartIndex += 3;
                break;
            }
        }
        /////////////////////////////END REACTIVE///////////////////////////////////

        int tariffSwitchover = rawData.indexOf(switchOverPrefix) + switchOverSize;

        int maximumDemand1EndIndex = rawData.indexOf(")", maximumDemand1StartIndex);
        int maximumDemand2EndIndex = rawData.indexOf(")", maximumDemand2StartIndex);
        int maximumDemand3EndIndex = rawData.indexOf(")", maximumDemand3StartIndex);
        int power1EndIndex = rawData.indexOf(")", power1StartIndex);
        int power2EndIndex = rawData.indexOf(")", power2StartIndex);
        int power3EndIndex = rawData.indexOf(")", power3StartIndex);
        int powerReactiveEndIndex = rawData.indexOf(")", powerReactiveStartIndex);

        meterValuesStruct.setMeterSerial(rawData.substring(serialStartIndex, serialStartIndex + ConstDefines.METER_SERIAL_SIZE));

        String cTm = rawData.substring(timeStartIndex, timeStartIndex + ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        meterValuesStruct.setMeterTime(cTm.substring(0, 0 + 2) + ":" + cTm.substring(2, 2 + 2) + ":" + cTm.substring(4, 4 + 2));
        String cDt = rawData.substring(dateStartIndex, dateStartIndex + ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        meterValuesStruct.setMeterDate( "20" + cDt.substring(5, 5 + 2) + "." + cDt.substring(3, 3 + 2) + "." + cDt.substring(1, 1 + 2));

        String counterDim1 = rawData.substring(maximumDemand1StartIndex, maximumDemand1EndIndex);
        String counterDim2 = rawData.substring(maximumDemand2StartIndex, maximumDemand2EndIndex);
        String counterDim3 = rawData.substring(maximumDemand3StartIndex, maximumDemand3EndIndex);

        Double mDim1 = Math.max(Double.parseDouble(counterDim1), Double.parseDouble(counterDim2));
        Double mDim2 = Math.max(Double.parseDouble(counterDim2), Double.parseDouble(counterDim3));
        Double mDim = Math.max(mDim1, mDim2);
        meterValuesStruct.setMeterMaximumDimand(String.valueOf(mDim));
        /////Taghirat: 94/05/13/////


        meterValuesStruct.setMeterPowerActiveT1(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterPowerActiveT2(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterPowerActiveT3(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterPowerReActive(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterPowerActiveT1(rawData.substring(power1StartIndex, power1EndIndex));
        meterValuesStruct.setMeterPowerActiveT2(rawData.substring(power2StartIndex, power2EndIndex));
        meterValuesStruct.setMeterPowerActiveT3(rawData.substring(power3StartIndex, power3EndIndex));
        meterValuesStruct.setMeterPowerActiveT4(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterPowerReActive(rawData.substring(powerReactiveStartIndex, powerReactiveEndIndex));

    }
}