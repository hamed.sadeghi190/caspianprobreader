package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterValuesStruct;
import ParsingCommonFiles.StringTravers.Jam110StringPrepare;

import static ParsingCommonFiles.Parsing.ConstDefines.JAM_110_NAME;



public class _JAM_110 extends  _Meter_Base {

    private int serialStartIndex = 12;
    private int timeStartIndex = 36;
    private int dateStartIndex = 12;
    private int power1StartIndex = 18;
    private int power2StartIndex = 42;
    private int power3StartIndex = 66;

    private int serialEndInx = 42;
    private int timeEndInx = 46;
    private int dateEndInx = 30;
    private int power1EndInx = 36;
    private int power2EndInx = 60;
    private int power3EndInx = 84;

    @Override
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        parsePowerData(rawData,meterValuesStruct);
    }
    private void parsePowerData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        int indOfParent = rawData.indexOf("5(");
        String powerActiveT1 = rawData.substring(indOfParent + 4, indOfParent + 10);
        String powerActiveT2 = rawData.substring(indOfParent + 12, indOfParent + 18);
        String powerActiveT3 = rawData.substring(indOfParent + 20, indOfParent + 26);
        meterValuesStruct.setMeterPowerActiveT1(Jam110StringPrepare.preparePowerValue(Jam110StringPrepare.translateData(powerActiveT1)));
        meterValuesStruct.setMeterPowerActiveT2(Jam110StringPrepare.preparePowerValue(Jam110StringPrepare.translateData(powerActiveT2)));
        meterValuesStruct.setMeterPowerActiveT3(Jam110StringPrepare.preparePowerValue(Jam110StringPrepare.translateData(powerActiveT3)));
        meterValuesStruct.setMeterPowerActiveT4(ConstDefines.DASH_CONSTANT);

    }
    private void parseDateTimeData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        int indOfParent = rawData.indexOf("5(");
        String counterTime = rawData.substring(indOfParent + timeStartIndex, indOfParent + timeEndInx);
        String counterDate = rawData.substring(indOfParent + dateStartIndex, indOfParent + dateEndInx);
        meterValuesStruct.setMeterTime(Jam110StringPrepare.prepareTimeValue(Jam110StringPrepare.translateData(counterTime)));
        meterValuesStruct.setMeterDate(Jam110StringPrepare.prepareDateValue(Jam110StringPrepare.translateData(counterDate)));
    }
    private void parseSerialData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        int indOfParent = rawData.indexOf("5(");
        String counterSerial = rawData.substring(indOfParent + serialStartIndex, indOfParent + serialEndInx);
        meterValuesStruct.setMeterSerial(Jam110StringPrepare.translateData(counterSerial));
    }

}