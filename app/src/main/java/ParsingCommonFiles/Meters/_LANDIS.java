package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME2;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME3;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME4;



public class _LANDIS extends  _Meter_Base {

@Override
    public void initial(){
        this.setMeterName(LANDIS_NAME);
        this.setMeterName2(LANDIS_NAME2);
        this.setMeterName3(LANDIS_NAME3);// Manual
        this.setMeterName4(LANDIS_NAME4);// Manual
        this.setMeterNameAll("LGZ");

        this.setMeterSerialPrefix("0.1(");
        this.setMeterTimePrefix("0-0:0.9.1(");
        this.setMeterDatePrefix("0-0:0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("1-1:3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.applyPower1OrderingCretria(true);
        this.setTimeLength(8);
        this.setDateLength(8);
        this.setMeterSerialPrefixSize(4);
        this.setMeterTimePrefixSize(10);
        this.setMeterDatePrefixSize(10);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(10);
        this.setMeterPowerOutPrefixSize(0);
}
}