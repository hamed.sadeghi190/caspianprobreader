package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM200T_NAME;

public class _JAM_200T extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(JAM200T_NAME);
        this.setMeterNameAll("JAM200T");

        this.setMeterSerialPrefix("0.0.0.255(");
        this.setMeterTimePrefix("96.20.6.255(");
        this.setMeterDatePrefix("96.20.6.255(");
        this.setMeterMaximumDemandPrefix("5.6.0.101(");
        this.setMeterPower1TotalPrefix("1.8.0.255(");
        this.setMeterPower1Prefix("5.8.1.101(");
        this.setMeterPower2Prefix("5.8.2.101(");
        this.setMeterPower3Prefix("5.8.3.101(");
        this.setMeterPower4Prefix("5.8.4.101(");
        this.setMeterPowerReActorPrefix("3.8.0.101(");
        this.setMeterPowerOutPrefix("2.7.0.255(");

        this.setMeterVoltagePhase1Prefix("32.7(");
        this.setMeterCurrentPhase1Prefix("31.7(");
        this.setMeterVoltagePhase2Prefix("");
        this.setMeterVoltagePhase3Prefix("");
        this.setMeterCurrentPhase2Prefix("");
        this.setMeterCurrentPhase3Prefix("");
        this.setMeterPowerFactor1Prefix("");
        this.setMeterPowerFactor2Prefix("");
        this.setMeterPowerFactor3Prefix("");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(14);
        this.setTimeLength(19);
        this.setTimeStartPoint(11);
        this.setDateLength(10);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_Eleven);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_Eleven );
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);

        this.setMeterDateTimePrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(5);
        this.setMeterCurrentPhasePrefixSize(5);
        this.setMeterPowerOutPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_TEN);

       CreateRt1Command();
    }

    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:1-0:15.8.1.101;");
        this.AddRt1CommandParameter("P02:1-0:15.8.2.101;");
        this.AddRt1CommandParameter("P03:1-0:15.8.3.101;");
        this.AddRt1CommandParameter("P04:1-0:15.6.0.101;");
        this.AddRt1CommandParameter("P05:0-0:96.20.6.255;");
        this.AddRt1CommandParameter("P06:1-0:0.0.0.255;");
        this.AddRt1CommandParameter("P07:1-0:3.8.0.101;");
        this.AddRt1CommandParameter("P08:1-0:32.7.0.255;");
        this.AddRt1CommandParameter("P09:1-0:31.7.0.255;");
    }
}