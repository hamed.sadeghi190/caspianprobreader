package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterValuesStruct;
import ParsingCommonFiles.StringTravers.Evan1010StringPrepare;



public class _EVAN_1010 extends  _Meter_Base {

    private int serialStartIndex = 6;
    private int timeStartIndex = 0;
    private int dateStartIndex = 0;
    private int power1StartIndex = 692;
    private int power2StartIndex = 729;
    private int power3StartIndex = 766;

    private int serialEndInx = 19;
    private int timeEndInx = 0;
    private int dateEndInx = 0;
    private int power1EndInx = 698;
    private int power2EndInx = 735;
    private int power3EndInx = 772;

    @Override
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        prepareIndexes(rawData);
        parseSerialData(rawData, meterValuesStruct);
        parseDateTimeData(rawData, meterValuesStruct);
        parsePowerData(rawData,meterValuesStruct);
    }

    private void prepareIndexes(String rawData) {
        if(rawData.contains("MSGA"))
        {

        }
    }

    private void parsePowerData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        String powerActiveT1 = rawData.substring(power1StartIndex, power1EndInx);
        String powerActiveT2 = rawData.substring(power2StartIndex, power2EndInx);
        String powerActiveT3 = rawData.substring(power3StartIndex, power3EndInx);
        meterValuesStruct.setMeterPowerActiveT1(Evan1010StringPrepare.preparePowerValue(powerActiveT1));
        meterValuesStruct.setMeterPowerActiveT2(Evan1010StringPrepare.preparePowerValue(powerActiveT2));
        meterValuesStruct.setMeterPowerActiveT3(Evan1010StringPrepare.preparePowerValue(powerActiveT3));
        meterValuesStruct.setMeterPowerActiveT4(ConstDefines.DASH_CONSTANT);

    }
    private void parseDateTimeData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        meterValuesStruct.setMeterTime(ConstDefines.DASH_CONSTANT);
        meterValuesStruct.setMeterDate(ConstDefines.DASH_CONSTANT);
    }
    private void parseSerialData(String rawData, MeterValuesStruct meterValuesStruct)
    {
        String counterSerial = rawData.substring(serialStartIndex, serialEndInx);
        meterValuesStruct.setMeterSerial(counterSerial);
    }

}