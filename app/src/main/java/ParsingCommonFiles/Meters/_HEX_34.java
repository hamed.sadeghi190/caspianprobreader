package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;



public class _HEX_34 extends  _Meter_Base {
    @Override
    public void initial(){

        this.setMeterSerialPrefix("C.1.0(");
        this.setMeterSerialPrefix1("0.0.0(");
        this.setMeterTimePrefix("0.9.1(");
        this.setMeterDatePrefix("0.9.2(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.2(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("32.7(");
        this.setMeterCurrentPhase1Prefix("31.7(");

        this.applyOrderingCretria(false);
        this.checkForSecondSerialPrefix(true);
        this.setSerialLength(12);
        this.setDateLength(8);
        this.setTimeLength(8);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterSerialPrefix1Size(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(6);
        this.setMeterDatePrefixSize(6);
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterVoltagePhasePrefixSize(5);
        this.setMeterCurrentPhasePrefixSize(5);
        this.setMeterPowerOutPrefixSize(0);
    }
}