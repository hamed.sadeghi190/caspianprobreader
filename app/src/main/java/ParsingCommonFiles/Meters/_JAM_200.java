package ParsingCommonFiles.Meters;


import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.JAM200_NAME;



public class _JAM_200 extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(JAM200_NAME);
        this.setMeterNameAll("JAM200");

        this.setMeterSerialPrefix("0.0.0(");
        this.setMeterTimePrefix("1.0.0(");
        this.setMeterDatePrefix("1.0.0(");
        this.setMeterMaximumDemandPrefix("1.6.0(");
        this.setMeterPower1TotalPrefix("1.8.0(");
        this.setMeterPower1Prefix("1.8.1(");
        this.setMeterPower2Prefix("1.8.2(");
        this.setMeterPower3Prefix("1.8.3(");
        this.setMeterPower4Prefix("1.8.4(");
        this.setMeterPowerReActorPrefix("3.8.0(");
        this.setMeterPowerOutPrefix("");
        this.setMeterVoltagePhase1Prefix("32.7(");
        this.setMeterCurrentPhase1Prefix("31.7(");
        this.setMeterVoltagePhase2Prefix("");
        this.setMeterVoltagePhase3Prefix("");
        this.setMeterCurrentPhase2Prefix("");
        this.setMeterCurrentPhase3Prefix("");
        this.setMeterPowerFactor1Prefix("");
        this.setMeterPowerFactor2Prefix("");
        this.setMeterPowerFactor3Prefix("");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(13);
        this.setTimeLength(17);
        this.setTimeStartPoint(9);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX );
        this.setMeterMaximumDemandPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPower4PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterPowerReActorPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);

        this.setMeterDateTimePrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(5);
        this.setMeterCurrentPhasePrefixSize(5);
        this.setMeterPowerOutPrefixSize(0);
        CreateRt1Command();

    }
    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:0.0.0;");
        this.AddRt1CommandParameter("P02:1.0.0;");
        this.AddRt1CommandParameter("P03:1.8.1;");
        this.AddRt1CommandParameter("P04:1.8.2;");
        this.AddRt1CommandParameter("P05:1.8.3;");
        this.AddRt1CommandParameter("P06:3.8.0;");
        this.AddRt1CommandParameter("P07:1.6.0;");
        this.AddRt1CommandParameter("P08:32.7;");
        this.AddRt1CommandParameter("P09:31.7;");
    }
}
