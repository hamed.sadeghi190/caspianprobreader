package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;
import ParsingCommonFiles.Parsing.MeterSpecialValuesStruct;
import ParsingCommonFiles.Parsing.MeterValuesStruct;

import static ParsingCommonFiles.Parsing.ConstDefines.ACE_5000_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.K_WATT_CONSTANT;



public class _ACE_5000 extends  _Meter_Base {
    @Override
    public void initial(){
        this.setMeterName(ACE_5000_NAME);
        this.setMeterNameAll("SLb5");

        this.setMeterSerialPrefix("C.1(");
        this.setMeterTimePrefix("1.0(1 ");
        this.setMeterDatePrefix("1.0(1 ");
        this.setMeterMaximumDemandPrefix("1.6(");
        this.setMeterPower1TotalPrefix("15.8(");
        this.setMeterPower1Prefix("15.8.1(");
        this.setMeterPower2Prefix("15.8.2(");
        this.setMeterPower3Prefix("15.8.3(");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("3.8(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("32.7(");
        this.setMeterVoltagePhase2Prefix("52.7(");
        this.setMeterVoltagePhase3Prefix("72.7(");
        this.setMeterCurrentPhase1Prefix("31.7(");
        this.setMeterCurrentPhase2Prefix("51.7(");
        this.setMeterCurrentPhase3Prefix("71.7(");
        this.setMeterPowerFactor1Prefix("33.7(");
        this.setMeterPowerFactor2Prefix("53.7(");
        this.setMeterPowerFactor3Prefix("73.7(");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(12);
        this.setTimeLength(8);
        this.setDateLength(8);
        this.setMeterSerialPrefixSize(4);
        this.setPowersPlaceHolder(1);
        this.setMeterDateTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX + 9);
        this.setMeterDatePrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterMaximumDemandPrefixSize(4);
        this.setMeterPower1PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        this.setMeterPower2PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        this.setMeterPower3PrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SEVEN);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(4);
        this.setMeterVoltagePhasePrefixSize(5);
        this.setMeterCurrentPhasePrefixSize(5);
        this.setMeterPowerFactorPrefixSize(5);
        this.setMeterPowerOutPrefixSize(0);
    }
    @Override
    public void parse(String rawData, MeterValuesStruct meterValuesStruct, int readType) {
        setSerialStartIndex(rawData);
        setTimeStartIndex(rawData);
        setDateStartIndex(rawData);
        setMaximumDemandStartEndIndex(rawData);
        setPower1StartEndIndex(rawData, K_WATT_CONSTANT);
        setPower2StartEndIndex(rawData, K_WATT_CONSTANT);
        setPower3StartEndIndex(rawData, K_WATT_CONSTANT);
        setPowerReactiveStartEndIndex(rawData);
        setValuesToStruct(rawData, meterValuesStruct, readType);
    }
    @Override
    public void parse(String rawData, MeterSpecialValuesStruct meterSpecialValuesStruct) {
        rawData = rawData.replace("\r\n","");
        rawData = rawData.replace(" ","");

        setDateTimeStartEndIndex(rawData);
        setVoltagePhase1StartEndIndex(rawData);
        setVoltagePhase2StartEndIndex(rawData);
        setVoltagePhase3StartEndIndex(rawData);
        setCurrentPhase1StartEndIndex(rawData);
        setCurrentPhase2StartEndIndex(rawData);
        setCurrentPhase3StartEndIndex(rawData);
        setPowerFactor1StartEndIndex(rawData);
        setPowerFactor2StartEndIndex(rawData);
        setPowerFactor3StartEndIndex(rawData);
        setValuesToSpecialStruct(rawData, meterSpecialValuesStruct);
    }

}