package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;



public class _IRANTEK_34 extends  _Meter_Base {

    @Override
    public void initial(){

        this.setMeterSerialPrefix("C.1.0(");
        this.setMeterTimePrefix("");
        this.setMeterDatePrefix("");
        this.setMeterMaximumDemandPrefix("1-0:1.6.0.255(");
        this.setMeterPower1TotalPrefix("1-0:1.8.0(");
        this.setMeterPower1Prefix("1-0:1.8.1.255(");
        this.setMeterPower2Prefix("1-0:1.8.2.255(");
        this.setMeterPower3Prefix("1-0:1.8.3.255(");
        this.setMeterPower4Prefix("1-0:1.8.4.255(");
        this.setMeterPowerReActorPrefix("1-0:3.8.0.255(");
        this.setMeterPowerOutPrefix("");

        this.setMeterVoltagePhase1Prefix("1-0:32.7.0.255(");
        this.setMeterVoltagePhase2Prefix("1-0:52.7.0.255(");
        this.setMeterVoltagePhase3Prefix("1-0:72.7.0.255(");
        this.setMeterCurrentPhase1Prefix("1-0:31.7.0.255(");
        this.setMeterCurrentPhase2Prefix("1-0:51.7.0.255(");
        this.setMeterCurrentPhase3Prefix("1-0:71.7.0.255(");

        this.applyPower1OrderingCretria(true);
        this.setSerialLength(16);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_SIX);
        this.setMeterTimePrefixSize(0);
        this.setMeterDatePrefixSize(0);
        this.setMeterMaximumDemandPrefixSize(14);
        this.setMeterPower1PrefixSize(14);
        this.setMeterPower2PrefixSize(14);
        this.setMeterPower3PrefixSize(14);
        this.setMeterPower4PrefixSize(14);
        this.setMeterPowerReActorPrefixSize(14);
        this.setMeterVoltagePhasePrefixSize(15);
        this.setMeterCurrentPhasePrefixSize(15);
        this.setMeterPowerOutPrefixSize(0);
        CreateRt1Command();
    }

    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:1-0:0.0.0.255;");
        this.AddRt1CommandParameter("P02:0-0:1.0.0.255;");
        this.AddRt1CommandParameter("P03:1-0:15.8.1.255;");
        this.AddRt1CommandParameter("P04:1-0:15.8.2.255;");
        this.AddRt1CommandParameter("P05:1-0:15.8.3.255;");
        this.AddRt1CommandParameter("P06:1-0:3.8.0.255;");
        this.AddRt1CommandParameter("P07:1-0:31.7.0.255;");
        this.AddRt1CommandParameter("P08:1-0:32.7.0.255;");
        this.AddRt1CommandParameter("P09:1-0:51.7.0.255;");
        this.AddRt1CommandParameter("P10:1-0:52.7.0.255;");
        this.AddRt1CommandParameter("P11:1-0:71.7.0.255;");
        this.AddRt1CommandParameter("P12:1-0:72.7.0.255;");
    }
}
