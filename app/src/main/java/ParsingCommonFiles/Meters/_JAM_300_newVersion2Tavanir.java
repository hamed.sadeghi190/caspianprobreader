package ParsingCommonFiles.Meters;

import ParsingCommonFiles.Parsing.ConstDefines;

import static ParsingCommonFiles.Parsing.ConstDefines.JAM_300_NEW_VERSION2_TAVANIR_NAME;



public class _JAM_300_newVersion2Tavanir extends  _Meter_Base {

    @Override
    public void initial(){
        this.setMeterName(JAM_300_NEW_VERSION2_TAVANIR_NAME);
        this.setMeterNameAll("JAM300");

        this.setMeterSerialPrefix("1.0.255(");
        this.setMeterTimePrefix("1.3.255(");
        this.setMeterDatePrefix("1.3.255(");
        this.setMeterMaximumDemandPrefix("1.6.0.255(");
        this.setMeterPower1TotalPrefix("1.8.0.255(");
        this.setMeterPower1Prefix("1.8.1.255(");
        this.setMeterPower2Prefix("1.8.2.255(");
        this.setMeterPower3Prefix("1.8.3.255(");
        this.setMeterPower4Prefix("");
        this.setMeterPowerReActorPrefix("3.8.0.255(");
        this.setMeterPowerOutPrefix("2.8.0.255(");

        this.setMeterVoltagePhase1Prefix("1-0:32.7.0.255(");
        this.setMeterVoltagePhase2Prefix("1-0:52.7.0.255(");
        this.setMeterVoltagePhase3Prefix("1-0:72.7.0.255(");
        this.setMeterCurrentPhase1Prefix("1-0:31.7.0.255(");
        this.setMeterCurrentPhase2Prefix("1-0:51.7.0.255(");
        this.setMeterCurrentPhase3Prefix("1-0:71.7.0.255(");
        this.setMeterPowerFactor1Prefix("");
        this.setMeterPowerFactor2Prefix("");
        this.setMeterPowerFactor3Prefix("");

        this.applyPower1OrderingCretria(true);
        this.setDateLength(10);
        this.setTimeLength(19);
        this.setTimeStartPoint(11);
        this.setMeterSerialPrefixSize(ConstDefines.DEFAULT_PREFIX_SIZE_EIGHT);
        this.setMeterTimePrefixSize(8);
        this.setMeterDatePrefixSize(8);
        this.setMeterMaximumDemandPrefixSize(10);
        this.setMeterPower1PrefixSize(10);
        this.setMeterPower2PrefixSize(10);
        this.setMeterPower3PrefixSize(10);
        this.setMeterPower4PrefixSize(0);
        this.setMeterPowerReActorPrefixSize(10);
        this.setMeterPowerOutPrefixSize(10);

        this.setMeterDateTimePrefixSize(0);
        this.setMeterVoltagePhasePrefixSize(15);
        this.setMeterCurrentPhasePrefixSize(15);
        CreateRt1Command();
    }

    private void CreateRt1Command() {
        this.AddRt1CommandParameter("RT1;");
        this.AddRt1CommandParameter("P01:0-0:96.1.0.255;");
        this.AddRt1CommandParameter("P02:0-0:96.1.3.255;");
        this.AddRt1CommandParameter("P03:1-0:1.8.1.255;");
        this.AddRt1CommandParameter("P04:1-0:1.8.2.255;");
        this.AddRt1CommandParameter("P05:1-0:1.8.3.255;");
        this.AddRt1CommandParameter("P06:1-0:1.8.0.255;");
        this.AddRt1CommandParameter("P07:1-0:32.7.0.255;");
        this.AddRt1CommandParameter("P08:1-0:52.7.0.255;");
        this.AddRt1CommandParameter("P09:1-0:72.7.0.255;");
        this.AddRt1CommandParameter("P10:1-0:31.7.0.255;");
        this.AddRt1CommandParameter("P11:1-0:51.7.0.255;");
        this.AddRt1CommandParameter("P12:1-0:71.7.0.255;");
    }
}
