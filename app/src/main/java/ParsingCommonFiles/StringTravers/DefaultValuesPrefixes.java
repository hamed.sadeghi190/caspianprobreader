package ParsingCommonFiles.StringTravers;



public class DefaultValuesPrefixes {
    private static String meterSerialPrefix = "";
    private static String meterSerialPrefix1 = "";
    private static String meterTimePrefix = "";
    private static String meterDatePrefix = "";
    private static String meterMaximumDemandPrefix = "";
    private static String meterMaximumDemand1Prefix = "";
    private static String meterMaximumDemand2Prefix = "";
    private static String meterPower1TotalPrefix= "";
    private static String meterPower1Prefix = "";
    private static String meterPower2Prefix = "";
    private static String meterPower3Prefix = "";
    private static String meterPower4Prefix = "";
    private static String meterPowerReActorPrefix = "";
    private static String meterPowerOutPrefix = "";

    public static void setMeterSerialPrefix(String meterSerialPrefix) {
        DefaultValuesPrefixes.meterSerialPrefix = meterSerialPrefix;
    }
    public static void setMeterSerialPrefix1(String meterSerialPrefix) {
        DefaultValuesPrefixes.meterSerialPrefix1 = meterSerialPrefix;
    }
    public static void setMeterTimePrefix(String meterTimePrefix) {
        DefaultValuesPrefixes.meterTimePrefix = meterTimePrefix;
    }

    public static void setMeterDatePrefix(String meterDatePrefix) {
        DefaultValuesPrefixes.meterDatePrefix = meterDatePrefix;
    }

    public static void setMeterMaximumDemandPrefix(String meterMaximumDemandPrefix) {
        DefaultValuesPrefixes.meterMaximumDemandPrefix = meterMaximumDemandPrefix;
    }

    public static void setMeterMaximumDemand1Prefix(String meterMaximumDemand1Prefix) {
        DefaultValuesPrefixes.meterMaximumDemand1Prefix = meterMaximumDemand1Prefix;
    }

    public static void setMeterMaximumDemand2Prefix(String meterMaximumDemand2Prefix) {
        DefaultValuesPrefixes.meterMaximumDemand2Prefix = meterMaximumDemand2Prefix;
    }

    public static void setMeterPower1TotalPrefix(String meterPower1TotalPrefix) {
        DefaultValuesPrefixes.meterPower1TotalPrefix = meterPower1TotalPrefix;
    }

    public static void setMeterPower1Prefix(String meterPower1Prefix) {
        DefaultValuesPrefixes.meterPower1Prefix = meterPower1Prefix;
    }

    public static void setMeterPower2Prefix(String meterPower2Prefix) {
        DefaultValuesPrefixes.meterPower2Prefix = meterPower2Prefix;
    }

    public static void setMeterPower3Prefix(String meterPower3Prefix) {
        DefaultValuesPrefixes.meterPower3Prefix = meterPower3Prefix;
    }

    public static void setMeterPower4Prefix(String meterPower4Prefix) {
        DefaultValuesPrefixes.meterPower4Prefix = meterPower4Prefix;
    }

    public static void setMeterPowerReActorPrefix(String meterPowerReActorPrefix) {
        DefaultValuesPrefixes.meterPowerReActorPrefix = meterPowerReActorPrefix;
    }

    public static String getMeterPower4Prefix() { return DefaultValuesPrefixes.meterPower4Prefix; }

    public static String getMeterMaximumDemand1Prefix() { return DefaultValuesPrefixes.meterMaximumDemand1Prefix; }

    public static String getMeterMaximumDemand2Prefix() { return DefaultValuesPrefixes.meterMaximumDemand2Prefix; }

    public static String getMeterPowerOutPrefix() {
        return meterPowerOutPrefix;
    }

    public static void setMeterPowerOutPrefix(String meterPowerOutPrefix) {
        DefaultValuesPrefixes.meterPowerOutPrefix = meterPowerOutPrefix;
    }

    public String getMeterSerialPrefix() {
        return DefaultValuesPrefixes.meterSerialPrefix;
    }

    public String getMeterSerialPrefix1() {
        return DefaultValuesPrefixes.meterSerialPrefix1;
    }
    public String getMeterTimePrefix() {
        return DefaultValuesPrefixes.meterTimePrefix;
    }

    public String getMeterDatePrefix() {
        return DefaultValuesPrefixes.meterDatePrefix;
    }

    public String getMeterMaximumDemandPrefix() {
        return DefaultValuesPrefixes.meterMaximumDemandPrefix;
    }

    public String getMeterPower1TotalPrefix() {
        return DefaultValuesPrefixes.meterPower1TotalPrefix;
    }

    public String getMeterPower1Prefix() {
        return DefaultValuesPrefixes.meterPower1Prefix;
    }

    public String getMeterPower2Prefix() {
        return DefaultValuesPrefixes.meterPower2Prefix;
    }

    public String getMeterPower3Prefix() {
        return DefaultValuesPrefixes.meterPower3Prefix;
    }

    public String getMeterPowerReActorPrefix() {
        return DefaultValuesPrefixes.meterPowerReActorPrefix;
    }

    public void resetAllFields() {
        meterSerialPrefix = "";
        meterTimePrefix = "";
        meterDatePrefix = "";
        meterMaximumDemandPrefix = "";
        meterMaximumDemand1Prefix = "";
        meterMaximumDemand2Prefix = "";
        meterPower1TotalPrefix= "";
        meterPower1Prefix = "";
        meterPower2Prefix = "";
        meterPower3Prefix = "";
        meterPower4Prefix = "";
        meterPowerReActorPrefix = "";
        meterPowerOutPrefix = "";
    }
}
