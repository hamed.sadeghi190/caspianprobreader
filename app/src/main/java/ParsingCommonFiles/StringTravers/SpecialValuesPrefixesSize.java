package ParsingCommonFiles.StringTravers;



public class SpecialValuesPrefixesSize {
    private static int meterDateTimePrefixSize = 0;
    private static int meterVoltagePhasePrefixSize = 0;
    private static int meterCurrentPhasePrefixSize = 0;
    private static int meterPowerFactorPrefixSize = 0;

    public static int getMeterVoltagePhasePrefixSize() {
        return SpecialValuesPrefixesSize.meterVoltagePhasePrefixSize;
    }

    public static int getMeterCurrentPhasePrefixSize() {
        return  SpecialValuesPrefixesSize.meterCurrentPhasePrefixSize;
    }

    public static int getMeterPowerFactorPrefixSize() {
        return SpecialValuesPrefixesSize.meterPowerFactorPrefixSize;
    }

    public static void setMeterVoltagePhasePrefixSize(int meterVoltagePhasePrefixSize) {
        SpecialValuesPrefixesSize.meterVoltagePhasePrefixSize = meterVoltagePhasePrefixSize;
    }

    public static void setMeterCurrentPhasePrefixSize(int meterCurrentPhasePrefixSize) {
        SpecialValuesPrefixesSize.meterCurrentPhasePrefixSize = meterCurrentPhasePrefixSize;
    }

    public static void setMeterPowerFactorPrefixSize(int meterPowerFactorPrefixSize) {
        SpecialValuesPrefixesSize.meterPowerFactorPrefixSize = meterPowerFactorPrefixSize;
    }

    public static int getMeterDateTimePrefixSize() {
        return meterDateTimePrefixSize;
    }

    public static void setMeterDateTimePrefixSize(int meterDateTimePrefixSize) {
        SpecialValuesPrefixesSize.meterDateTimePrefixSize = meterDateTimePrefixSize;
    }

    public void resetAllFields() {
        meterDateTimePrefixSize = 0;
        meterVoltagePhasePrefixSize = 0;
        meterCurrentPhasePrefixSize = 0;
        meterPowerFactorPrefixSize = 0;
    }
}
