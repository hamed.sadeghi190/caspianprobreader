package ParsingCommonFiles.StringTravers;



public class DefaultValuesIndexes {
    private static int serialStartIndex = -1;
    private static int timeStartIndex = -1;
    private static int timePlaceHolder = 0;
    private static int dateStartIndex = -1;
    private static int serialLength = -1;
    private static int timeLength = -1;
    private static int timeStartPoint = 0;
    private static int dateLength = -1;
    private static Boolean dateReverse = false;
    private static int maximumDemandStartIndex = -1;
    private static int powersPlaceHolder = 0;
    private static int power1StartIndex = -1;
    private static int power2StartIndex  = -1;
    private static int power3StartIndex  = -1;
    private static int power4StartIndex  = -1;
    private static int powerTotalStartIndex  = -1;
    private static int TariffSwitchoverStartIndex = -1;
    private static int powerReactorStartIndex = -1;
    private static int powerOutStartIndex = -1;
    private static int maximumDemandEndIndex = -1;
    private static int power1EndIndex = -1;
    private static int power2EndIndex = -1;
    private static int power3EndIndex = -1;
    private static int power4EndIndex = -1;
    private static int powerReactorEndIndex = -1;
    private static int powerTotalEndIndex = -1;
    private static int powerOutEndIndex = -1;

    public static int getPowersPlaceHolder() {
        return powersPlaceHolder;
    }

    public static void setPowersPlaceHolder(int powersPlaceHolder) {
        DefaultValuesIndexes.powersPlaceHolder = powersPlaceHolder;
    }

    public static int getTimePlaceHolder() {
        return timePlaceHolder;
    }

    public static void setTimePlaceHolder(int timePlaceHolder) {
        DefaultValuesIndexes.timePlaceHolder = timePlaceHolder;
    }

    public static int getTimeStartPoint() {
        return timeStartPoint;
    }

    public static void setTimeStartPoint(int timeStartPoint) {
        DefaultValuesIndexes.timeStartPoint = timeStartPoint;
    }

    public static int getPowerOutStartIndex() {
        return powerOutStartIndex;
    }

    public static void setPowerOutStartIndex(int powerOutStartIndex) {
        DefaultValuesIndexes.powerOutStartIndex = powerOutStartIndex;
    }

    public static int getPowerOutEndIndex() {
        return powerOutEndIndex;
    }

    public static void setPowerOutEndIndex(int powerOutEndIndex) {
        DefaultValuesIndexes.powerOutEndIndex = powerOutEndIndex;
    }

    public int getSerialStartIndex() { return DefaultValuesIndexes.serialStartIndex; }

    public void setSerialStartIndex(int serialStartIndex) { DefaultValuesIndexes.serialStartIndex = serialStartIndex; }

    public int getTimeStartIndex() { return DefaultValuesIndexes.timeStartIndex; }

    public void setTimeStartIndex(int timeStartIndex) { DefaultValuesIndexes.timeStartIndex = timeStartIndex; }

    public int getDateStartIndex() { return DefaultValuesIndexes.dateStartIndex; }

    public void setDateStartIndex(int dateStartIndex) { DefaultValuesIndexes.dateStartIndex = dateStartIndex; }

    public int getMaximumDemandStartIndex() { return DefaultValuesIndexes.maximumDemandStartIndex; }

    public void setMaximumDemandStartIndex(int maximumDemandStartIndex) { DefaultValuesIndexes.maximumDemandStartIndex = maximumDemandStartIndex; }

    public int getPower1StartIndex() { return DefaultValuesIndexes.power1StartIndex; }

    public void setPower1StartIndex(int power1StartIndex) { DefaultValuesIndexes.power1StartIndex = power1StartIndex; }

    public int getPower2StartIndex() { return DefaultValuesIndexes.power2StartIndex; }

    public void setPower2StartIndex(int power2StartIndex) { DefaultValuesIndexes.power2StartIndex = power2StartIndex; }

    public int getPower3StartIndex() { return DefaultValuesIndexes.power3StartIndex; }

    public void setPower3StartIndex(int power3StartIndex) { DefaultValuesIndexes.power3StartIndex = power3StartIndex; }

    public int getPower4StartIndex() { return DefaultValuesIndexes.power4StartIndex; }

    public void setPower4StartIndex(int power4StartIndex) { DefaultValuesIndexes.power4StartIndex = power4StartIndex; }

    public int getPowerTotalStartIndex() { return DefaultValuesIndexes.powerTotalStartIndex; }

    public void setPowerTotalStartIndex(int powerTotalStartIndex) { DefaultValuesIndexes.powerTotalStartIndex = powerTotalStartIndex; }

    public int getTariffSwitchoverStartIndex() { return DefaultValuesIndexes.TariffSwitchoverStartIndex; }

    public void setTariffSwitchoverStartIndex(int tariffSwitchoverStartIndex) { DefaultValuesIndexes.TariffSwitchoverStartIndex = tariffSwitchoverStartIndex; }

    public int getPowerReactorStartIndex() { return DefaultValuesIndexes.powerReactorStartIndex; }

    public void setPowerReactorStartIndex(int powerReactorStartIndex) { DefaultValuesIndexes.powerReactorStartIndex = powerReactorStartIndex; }

    public int getMaximumDemandEndIndex() { return DefaultValuesIndexes.maximumDemandEndIndex; }

    public void setMaximumDemandEndIndex(int maximumDemandEndIndex) { DefaultValuesIndexes.maximumDemandEndIndex = maximumDemandEndIndex; }

    public int getPower1EndIndex() { return DefaultValuesIndexes.power1EndIndex; }

    public void setPower1EndIndex(int power1EndIndex) { DefaultValuesIndexes.power1EndIndex = power1EndIndex; }

    public int getPower2EndIndex() { return DefaultValuesIndexes.power2EndIndex; }

    public void setPower2EndIndex(int power2EndIndex) { DefaultValuesIndexes.power2EndIndex = power2EndIndex; }

    public int getPower3EndIndex() { return DefaultValuesIndexes.power3EndIndex; }

    public void setPower3EndIndex(int power3EndIndex) { DefaultValuesIndexes.power3EndIndex = power3EndIndex; }

    public int getPower4EndIndex() { return DefaultValuesIndexes.power4EndIndex; }

    public void setPower4EndIndex(int power4EndIndex) { DefaultValuesIndexes.power4EndIndex = power4EndIndex; }

    public int getPowerReactorEndIndex() { return DefaultValuesIndexes.powerReactorEndIndex; }

    public void setPowerReactorEndIndex(int powerReactorEndIndex) { DefaultValuesIndexes.powerReactorEndIndex = powerReactorEndIndex; }

    public int getPowerTotalEndIndex() { return DefaultValuesIndexes.powerTotalEndIndex; }

    public void setPowerTotalEndIndex(int powerTotalEndIndex) { DefaultValuesIndexes.powerTotalEndIndex = powerTotalEndIndex; }

    public void setSerialLength(int length) {
        serialLength = length;
    }

    public void setTimeLength(int length) {
        timeLength = length;
    }

    public void setDateLength(int length) {
        dateLength = length;
    }

    public void setDateReverse(boolean isDateReverse)
    {
        dateReverse = isDateReverse;
    }

    public int getSerialLength() {
        return serialLength;
    }

    public int getTimeLength() {
        return timeLength;
    }

    public int getDateLength() {
        return dateLength;
    }

    public Boolean getDateReverse(){ return dateReverse; }

    public void resetAllFields(){
        serialStartIndex = -1;
        timeStartIndex = -1;
        timePlaceHolder = 0;
        dateStartIndex = -1;
        serialLength = -1;
        timeLength = -1;
        timeStartPoint = 0;
        dateLength = -1;
        maximumDemandStartIndex = -1;
        powersPlaceHolder = 0;
        power1StartIndex = -1;
        power2StartIndex  = -1;
        power3StartIndex  = -1;
        power4StartIndex  = -1;
        powerTotalStartIndex  = -1;
        TariffSwitchoverStartIndex = -1;
        powerReactorStartIndex = -1;
        powerOutStartIndex = -1;
        maximumDemandEndIndex = -1;
        power1EndIndex = -1;
        power2EndIndex = -1;
        power3EndIndex = -1;
        power4EndIndex = -1;
        powerReactorEndIndex = -1;
        powerTotalEndIndex = -1;
        powerOutEndIndex = -1;
    }
}
