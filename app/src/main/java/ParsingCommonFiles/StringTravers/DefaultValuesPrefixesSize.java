package ParsingCommonFiles.StringTravers;



public class DefaultValuesPrefixesSize {
    private static int meterSerialPrefixSize = 0;
    private static int meterSerialPrefix1Size = 0;
    private static int meterTimePrefixSize = 0;
    private static int meterDatePrefixSize = 0;
    private static int meterMaximumDemandPrefixSize = 0;
    private static int meterPower1PrefixSize = 0;
    private static int meterPower2PrefixSize = 0;
    private static int meterPower3PrefixSize = 0;
    private static int meterPower4PrefixSize = 0;
    private static int meterPowerReActorPrefixSize = 0;
    private static int meterPowerOutPrefixSize = 0;

    public static void setMeterSerialPrefixSize(int meterSerialPrefixSize) {
        DefaultValuesPrefixesSize.meterSerialPrefixSize = meterSerialPrefixSize;
    }

    public static void setMeterSerialPrefix1Size(int meterSerialPrefixSize) {
        DefaultValuesPrefixesSize.meterSerialPrefix1Size = meterSerialPrefixSize;
    }

    public static void setMeterTimePrefixSize(int meterTimePrefixSize) {
        DefaultValuesPrefixesSize.meterTimePrefixSize = meterTimePrefixSize;
    }

    public static void setMeterDatePrefixSize(int meterDatePrefixSize) {
        DefaultValuesPrefixesSize.meterDatePrefixSize = meterDatePrefixSize;
    }

    public static void setMeterMaximumDemandPrefixSize(int meterMaximumDemandPrefixSize) {
        DefaultValuesPrefixesSize.meterMaximumDemandPrefixSize = meterMaximumDemandPrefixSize;
    }

    public static void setMeterPower1PrefixSize(int meterPower1PrefixSize) {
        DefaultValuesPrefixesSize.meterPower1PrefixSize = meterPower1PrefixSize;
    }

    public static void setMeterPower2PrefixSize(int meterPower2PrefixSize) {
        DefaultValuesPrefixesSize.meterPower2PrefixSize = meterPower2PrefixSize;
    }

    public static void setMeterPower3PrefixSize(int meterPower3PrefixSize) {
        DefaultValuesPrefixesSize.meterPower3PrefixSize = meterPower3PrefixSize;
    }

    public static void setMeterPower4PrefixSize(int meterPower4PrefixSize) {
        DefaultValuesPrefixesSize.meterPower4PrefixSize = meterPower4PrefixSize;
    }

    public static void setMeterPowerReActorPrefixSize(int meterPowerReActorPrefixSize) {
        DefaultValuesPrefixesSize.meterPowerReActorPrefixSize = meterPowerReActorPrefixSize;
    }

    public static int getMeterPower4PrefixSize() { return DefaultValuesPrefixesSize.meterPower4PrefixSize; }

    public static int getMeterPowerOutPrefixSize() {
        return meterPowerOutPrefixSize;
    }

    public static void setMeterPowerOutPrefixSize(int meterPowerOutPrefixSize) {
        DefaultValuesPrefixesSize.meterPowerOutPrefixSize = meterPowerOutPrefixSize;
    }

    public int getMeterSerialPrefixSize() {
        return DefaultValuesPrefixesSize.meterSerialPrefixSize;
    }

    public int getMeterSerialPrefix1Size() {
        return DefaultValuesPrefixesSize.meterSerialPrefix1Size;
    }

    public int getMeterTimePrefixSize() {
        return DefaultValuesPrefixesSize.meterTimePrefixSize;
    }

    public int getMeterDatePrefixSize() {
        return DefaultValuesPrefixesSize.meterDatePrefixSize;
    }

    public int getMeterMaximumDemandPrefixSize() {
        return DefaultValuesPrefixesSize.meterMaximumDemandPrefixSize;
    }

    public int getMeterPower1PrefixSize() {
        return DefaultValuesPrefixesSize.meterPower1PrefixSize;
    }

    public int getMeterPower2PrefixSize() {
        return DefaultValuesPrefixesSize.meterPower2PrefixSize;
    }

    public int getMeterPower3PrefixSize() {
        return DefaultValuesPrefixesSize.meterPower3PrefixSize;
    }

    public int getMeterPowerReActorPrefixSize() {
        return DefaultValuesPrefixesSize.meterPowerReActorPrefixSize;
    }

    public void resetAllFields() {
        meterSerialPrefixSize = 0;
        meterTimePrefixSize = 0;
        meterDatePrefixSize = 0;
        meterMaximumDemandPrefixSize = 0;
        meterPower1PrefixSize = 0;
        meterPower2PrefixSize = 0;
        meterPower3PrefixSize = 0;
        meterPower4PrefixSize = 0;
        meterPowerReActorPrefixSize = 0;
        meterPowerOutPrefixSize = 0;
    }
}
