package ParsingCommonFiles.StringTravers;



public class SpecialValuesPrefixes {
    private static String meterSpecialDateTimePrefix   = "";
    private static String meterVoltPhase1Prefix = "";
    private static String meterVoltPhase2Prefix = "";
    private static String meterVoltPhase3Prefix = "";
    private static String meterCurrentPhase1Prefix = "";
    private static String meterCurrentPhase2Prefix = "";
    private static String meterCurrentPhase3Prefix = "";
    private static String meterPowerFactor1Prefix = "";
    private static String meterPowerFactor2Prefix = "";
    private static String meterPowerFactor3Prefix = "";

    public static void setMeterDateTimePrefix(String meterDateTimePrefix) {
        SpecialValuesPrefixes.meterSpecialDateTimePrefix = meterDateTimePrefix;
    }

    public static String getMeterDateTimePrefix() {
        return SpecialValuesPrefixes.meterSpecialDateTimePrefix;
    }

    public static void setMeterCurrentPhase1Prefix(String meterCurrentPhase1Prefix) {
        SpecialValuesPrefixes.meterCurrentPhase1Prefix = meterCurrentPhase1Prefix;
    }

    public static void setMeterCurrentPhase2Prefix(String meterCurrentPhase2Prefix) {
        SpecialValuesPrefixes.meterCurrentPhase2Prefix = meterCurrentPhase2Prefix;
    }

    public static void setMeterCurrentPhase3Prefix(String meterCurrentPhase3Prefix) {
        SpecialValuesPrefixes.meterCurrentPhase3Prefix = meterCurrentPhase3Prefix;
    }
    public static String getMeterVoltagePhase1Prefix() {
        return SpecialValuesPrefixes.meterVoltPhase1Prefix;
    }
    public static String getMeterVoltagePhase2Prefix() {
        return SpecialValuesPrefixes.meterVoltPhase2Prefix;
    }
    public static String getMeterVoltagePhase3Prefix() {
        return SpecialValuesPrefixes.meterVoltPhase3Prefix;
    }

    public static String getMeterCurrentPhase1Prefix() {
        return SpecialValuesPrefixes.meterCurrentPhase1Prefix;
    }
    public static String getMeterCurrentPhase2Prefix() {
        return SpecialValuesPrefixes.meterCurrentPhase2Prefix;
    }
    public static String getMeterCurrentPhase3Prefix() {
        return SpecialValuesPrefixes.meterCurrentPhase3Prefix;
    }

    public static String getMeterPowerFactor1Prefix() {
        return SpecialValuesPrefixes.meterPowerFactor1Prefix;
    }
    public static String getMeterPowerFactor2Prefix() {
        return SpecialValuesPrefixes.meterPowerFactor2Prefix;
    }
    public static String getMeterPowerFactor3Prefix() {
        return SpecialValuesPrefixes.meterPowerFactor3Prefix;
    }

    public static void setMeterVoltagePhase1Prefix(String meterVoltagePhase1Prefix) {
        SpecialValuesPrefixes.meterVoltPhase1Prefix = meterVoltagePhase1Prefix;
    }

    public static void setMeterVoltagePhase2Prefix(String meterVoltagePhase2Prefix) {
        SpecialValuesPrefixes.meterVoltPhase2Prefix = meterVoltagePhase2Prefix;
    }

    public static void setMeterVoltagePhase3Prefix(String meterVoltagePhase3Prefix) {
        SpecialValuesPrefixes.meterVoltPhase3Prefix = meterVoltagePhase3Prefix;
    }
    public static void setMeterPowerFactor1Prefix(String meterPowerFactor1Prefix) {
        SpecialValuesPrefixes.meterPowerFactor1Prefix = meterPowerFactor1Prefix;
    }

    public static void setMeterPowerFactor2Prefix(String meterPowerFactor2Prefix) {
        SpecialValuesPrefixes.meterPowerFactor2Prefix = meterPowerFactor2Prefix;
    }

    public static void setMeterPowerFactor3Prefix(String meterPowerFactor3Prefix) {
        SpecialValuesPrefixes.meterPowerFactor3Prefix = meterPowerFactor3Prefix;
    }

    public void resetAllFields() {
        meterSpecialDateTimePrefix   = "";
        meterVoltPhase1Prefix = "";
        meterVoltPhase2Prefix = "";
        meterVoltPhase3Prefix = "";
        meterCurrentPhase1Prefix = "";
        meterCurrentPhase2Prefix = "";
        meterCurrentPhase3Prefix = "";
        meterPowerFactor1Prefix = "";
        meterPowerFactor2Prefix = "";
        meterPowerFactor3Prefix = "";
    }
}
