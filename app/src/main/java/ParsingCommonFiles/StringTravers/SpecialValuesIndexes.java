package ParsingCommonFiles.StringTravers;



public class SpecialValuesIndexes {
    private static int dateTimeStartIndex = -1;
    private static int voltagePhase1StartIndex = -1;
    private static int voltagePhase2StartIndex  = -1;
    private static int voltagePhase3StartIndex  = -1;
    private static int currentPhase1StartIndex = -1;
    private static int currentPhase2StartIndex = -1;
    private static int currentPhase3StartIndex = -1;
    private static int powerFactor1StartIndex = -1;
    private static int powerFactor2StartIndex = -1;
    private static int powerFactor3StartIndex = -1;
    private static int dateTimeEndIndex = -1;
    private static int voltagePhase1EndIndex = -1;
    private static int voltagePhase2EndIndex = -1;
    private static int voltagePhase3EndIndex = -1;
    private static int currentPhase1EndIndex = -1;
    private static int currentPhase2EndIndex = -1;
    private static int currentPhase3EndIndex = -1;
    private static int powerFactor1EndIndex = -1;
    private static int powerFactor2EndIndex = -1;
    private static int powerFactor3EndIndex = -1;

    public static int getDateTimeStartIndex() {
        return SpecialValuesIndexes.dateTimeStartIndex;
    }

    public static void setDateTimeStartIndex(int dateTimeStartIndex) {
        SpecialValuesIndexes.dateTimeStartIndex = dateTimeStartIndex;
    }

    public static int getVoltagePhase1StartIndex() {
        return SpecialValuesIndexes.voltagePhase1StartIndex;
    }

    public static void setVoltagePhase1StartIndex(int voltagePhase1StartIndex) {
        SpecialValuesIndexes.voltagePhase1StartIndex = voltagePhase1StartIndex;
    }

    public static int getVoltagePhase2StartIndex() {
        return SpecialValuesIndexes.voltagePhase2StartIndex;
    }

    public static void setVoltagePhase2StartIndex(int voltagePhase2StartIndex) {
        SpecialValuesIndexes.voltagePhase2StartIndex = voltagePhase2StartIndex;
    }

    public static int getVoltagePhase3StartIndex() {
        return SpecialValuesIndexes.voltagePhase3StartIndex;
    }

    public static void setVoltagePhase3StartIndex(int voltagePhase3StartIndex) {
        SpecialValuesIndexes.voltagePhase3StartIndex = voltagePhase3StartIndex;
    }

    public static int getCurrentPhase1StartIndex() {
        return SpecialValuesIndexes.currentPhase1StartIndex;
    }

    public static void setCurrentPhase1StartIndex(int currentPhase1StartIndex) {
        SpecialValuesIndexes.currentPhase1StartIndex = currentPhase1StartIndex;
    }

    public static int getCurrentPhase2StartIndex() {
        return SpecialValuesIndexes.currentPhase2StartIndex;
    }

    public static void setCurrentPhase2StartIndex(int currentPhase2StartIndex) {
        SpecialValuesIndexes.currentPhase2StartIndex = currentPhase2StartIndex;
    }

    public static int getCurrentPhase3StartIndex() {
        return SpecialValuesIndexes.currentPhase3StartIndex;
    }

    public static void setCurrentPhase3StartIndex(int currentPhase3StartIndex) {
        SpecialValuesIndexes.currentPhase3StartIndex = currentPhase3StartIndex;
    }

    public static int getPowerFactor1StartIndex() {
        return SpecialValuesIndexes.powerFactor1StartIndex;
    }

    public static void setPowerFactor1StartIndex(int powerFactor1StartIndex) {
        SpecialValuesIndexes.powerFactor1StartIndex = powerFactor1StartIndex;
    }

    public static int getPowerFactor2StartIndex() {
        return SpecialValuesIndexes.powerFactor2StartIndex;
    }

    public static void setPowerFactor2StartIndex(int powerFactor2StartIndex) {
        SpecialValuesIndexes.powerFactor2StartIndex = powerFactor2StartIndex;
    }

    public static int getPowerFactor3StartIndex() {
        return SpecialValuesIndexes.powerFactor3StartIndex;
    }

    public static void setPowerFactor3StartIndex(int powerFactor3StartIndex) {
        SpecialValuesIndexes.powerFactor3StartIndex = powerFactor3StartIndex;
    }

    public static int getDateTimeEndIndex() {
        return SpecialValuesIndexes.dateTimeEndIndex;
    }

    public static void setDateTimeEndIndex(int dateTimeEndIndex) {
        SpecialValuesIndexes.dateTimeEndIndex = dateTimeEndIndex;
    }

    public static int getVoltagePhase1EndIndex() {
        return SpecialValuesIndexes.voltagePhase1EndIndex;
    }

    public static void setVoltagePhase1EndIndex(int voltagePhase1EndIndex) {
        SpecialValuesIndexes.voltagePhase1EndIndex = voltagePhase1EndIndex;
    }

    public static int getVoltagePhase2EndIndex() {
        return SpecialValuesIndexes.voltagePhase2EndIndex;
    }

    public static void setVoltagePhase2EndIndex(int voltagePhase2EndIndex) {
        SpecialValuesIndexes.voltagePhase2EndIndex = voltagePhase2EndIndex;
    }

    public static int getVoltagePhase3EndIndex() {
        return SpecialValuesIndexes.voltagePhase3EndIndex;
    }

    public static void setVoltagePhase3EndIndex(int voltagePhase3EndIndex) {
        SpecialValuesIndexes.voltagePhase3EndIndex = voltagePhase3EndIndex;
    }

    public static int getCurrentPhase1EndIndex() {
        return SpecialValuesIndexes.currentPhase1EndIndex;
    }

    public static void setCurrentPhase1EndIndex(int currentPhase1EndIndex) {
        SpecialValuesIndexes.currentPhase1EndIndex = currentPhase1EndIndex;
    }

    public static int getCurrentPhase2EndIndex() {
        return SpecialValuesIndexes.currentPhase2EndIndex;
    }

    public static void setCurrentPhase2EndIndex(int currentPhase2EndIndex) {
        SpecialValuesIndexes.currentPhase2EndIndex = currentPhase2EndIndex;
    }

    public static int getCurrentPhase3EndIndex() {
        return SpecialValuesIndexes.currentPhase3EndIndex;
    }

    public static void setCurrentPhase3EndIndex(int currentPhase3EndIndex) {
        SpecialValuesIndexes.currentPhase3EndIndex = currentPhase3EndIndex;
    }

    public static int getPowerFactor1EndIndex() {
        return SpecialValuesIndexes.powerFactor1EndIndex;
    }

    public static void setPowerFactor1EndIndex(int powerFactor1EndIndex) {
        SpecialValuesIndexes.powerFactor1EndIndex = powerFactor1EndIndex;
    }

    public static int getPowerFactor2EndIndex() {
        return SpecialValuesIndexes.powerFactor2EndIndex;
    }

    public static void setPowerFactor2EndIndex(int powerFactor2EndIndex) {
        SpecialValuesIndexes.powerFactor2EndIndex = powerFactor2EndIndex;
    }

    public static int getPowerFactor3EndIndex() {
        return SpecialValuesIndexes.powerFactor3EndIndex;
    }

    public static void setPowerFactor3EndIndex(int powerFactor3EndIndex) {
        SpecialValuesIndexes.powerFactor3EndIndex = powerFactor3EndIndex;
    }

    public void resetAllFields() {
        dateTimeStartIndex = -1;
        voltagePhase1StartIndex = -1;
        voltagePhase2StartIndex  = -1;
        voltagePhase3StartIndex  = -1;
        currentPhase1StartIndex = -1;
        currentPhase2StartIndex = -1;
        currentPhase3StartIndex = -1;
        powerFactor1StartIndex = -1;
        powerFactor2StartIndex = -1;
        powerFactor3StartIndex = -1;
        dateTimeEndIndex = -1;
        voltagePhase1EndIndex = -1;
        voltagePhase2EndIndex = -1;
        voltagePhase3EndIndex = -1;
        currentPhase1EndIndex = -1;
        currentPhase2EndIndex = -1;
        currentPhase3EndIndex = -1;
        powerFactor1EndIndex = -1;
        powerFactor2EndIndex = -1;
        powerFactor3EndIndex = -1;
    }
}
