package ParsingCommonFiles.StringTravers;



public class NamePrefixes {
        private static  String meterName = "";
        private static String meterName2 = "";
        private static String meterName3 = "";
        private static String meterName4 = "";
        private static String meterNameAll = "";

    public static void setMeterName(String meterName) {
        NamePrefixes.meterName = meterName;
    }

    public static void setMeterName2(String meterName2) {
        NamePrefixes.meterName2 = meterName2;
    }

    public static void setMeterName3(String meterName3) {
        NamePrefixes.meterName3 = meterName3;
    }

    public static void setMeterName4(String meterName4) {
        NamePrefixes.meterName4 = meterName4;
    }

    public static void setMeterNameAll(String meterNameAll) { NamePrefixes.meterNameAll = meterNameAll; }

    public static String getMeterName() {
        return NamePrefixes.meterName;
    }

    public static String getMeterNameAll() {
        return NamePrefixes.meterNameAll;
    }

    public static String getMeterName2() { return NamePrefixes.meterName2;  }

    public static String getMeterName3() { return NamePrefixes.meterName3;  }

    public static String getMeterName4() { return NamePrefixes.meterName4; }

    public void resetAllFields() {
        meterName = "";
        meterName2 = "";
        meterName3 = "";
        meterName4 = "";
        meterNameAll = "";
    }
}
