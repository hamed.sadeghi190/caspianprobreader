package ParsingCommonFiles.StringTravers;

import android.util.Log;


public class Evan1010StringPrepare {

    public static String SwapString(String Input) {
        int len = Input.length();
        char[] c = Input.toCharArray();
        try{
            for (int i = 0; i < len - 1; i = i + 2) {
                char temp = c[i];
                c[i] = c[i + 1];
                c[i + 1] = temp;
            }
        }catch(Exception e){
            Log.e("Error in swap",Input);}
        finally{return new String(c);}
    }
    public static String hexStringToAsciiString(String HEXInput)  {
        if(HEXInput.length()>0){
            String HI = HEXInput;
            int retVal = 0;
            if(HEXInput.contains("("))
            {
                HI = HEXInput.substring(HEXInput.indexOf("(")+1,HEXInput.indexOf(")")-1);
            }
            try{
                retVal = Integer.valueOf(HI,16);
            }
            catch(Exception e){
                Log.e("Converter",e.toString());Log.e("HExInput",HEXInput);Log.e("str",HI);
            }
            return Integer.toString(retVal);
        }
        else{
            return "0";
        }
    }
    public static String hexStringToAsciiStringPow(String HEXInput) {
        if(HEXInput.length() > 0){
            int dec = Integer.parseInt(HEXInput, 16);
            return Integer.toString(dec);}
        else{
            return "0";
        }
    }
    public static String translatePowerData(String input) {
        if(input.length() > 0){
            String data = SwapString(new String(input.replace(" ", "")));
            return hexStringToAsciiStringPow(data);
        }else{
            return "";
        }
    }
    public static String prepareTimeValue(String HexTime) {
        if(HexTime.length()>0){
            String hour = HexTime.substring(0, 2);
            String minit = HexTime.substring(2, 4);
            String sec = HexTime.substring(4, 6);
            String ClearTime = hour + ":" + minit + ":" + sec;
            return ClearTime;
        }else{
            return "";
        }
    }
    public static String prepareDateValue(String HexDate) {
        if(HexDate.length()>0){
            String year = HexDate.substring(0, 2);
            String month = HexDate.substring(2, 4);
            String day = HexDate.substring(4, 6);
            String ClearDate = year + "/" + month + "/" + day;
            return ClearDate;
        }else{
            return "";
        }
    }

    public static String preparePowerValue(String HexPOWER) {
        if(HexPOWER.length()>0){
            String ClearPow = Integer.parseInt(HexPOWER, 16)+"";
            String pw = ClearPow.substring(0, ClearPow.length() - 2) + "."
                    + ClearPow.substring(ClearPow.length() - 2, ClearPow.length());
            return pw;
        }
        else{
            return "";
        }
    }

    public static String returnEvan1010Time(String Evan1010_DateTime) {
        String ReturnedValue = prepareTimeValue(getTimeORDateFromDateTimeString(2, Evan1010_DateTime));
        return ReturnedValue;
    }

    public static String returnEvan1010Date(String Evan1010_DateTime) {
        String ReturnedValue = prepareDateValue(getTimeORDateFromDateTimeString(1, Evan1010_DateTime));
        return ReturnedValue;
    }

    public static String returnEvan1010LowTraffic(String Evan1010_Traffic) {
        String ReturnedValue = preparePowerValue(translatePowerData(getCustomTrafficFromString(1, Evan1010_Traffic)));
        return ReturnedValue;
    }

    public static String returnEvan1010MidTraffic(String Evan1010_Traffic) {
        String ReturnedValue = preparePowerValue(translatePowerData(getCustomTrafficFromString(
                2, Evan1010_Traffic)));
        return ReturnedValue;
    }

    public static String returnEvan1010HighTraffic(String Evan1010_Traffic) {
        String ReturnedValue = preparePowerValue(translatePowerData(getCustomTrafficFromString(3, Evan1010_Traffic)));
        return ReturnedValue;
    }

    private static String getTimeORDateFromDateTimeString(int DateOrTimeString, String strDateTime) {
        int timeStartIndex = 4;
        int dateStartIndex = 12;
        int TDLENGTH = 6;

        String stDT = "";
        switch (DateOrTimeString) {
            case 1:
                if(strDateTime.length() > timeStartIndex+TDLENGTH){
                    stDT = strDateTime.substring(timeStartIndex, timeStartIndex + TDLENGTH);}
                break;
            case 2:
                if(strDateTime.length() > dateStartIndex+TDLENGTH){
                    stDT = strDateTime.substring(dateStartIndex, dateStartIndex + TDLENGTH);}
                break;
        }
        return stDT;
    }

    private static String getCustomTrafficFromString(int TrafficType, String strTraffic) {
        int lowTrafficIndex = 6;
        int midTrafficIndex = 14;
        int highTrafficIndex = 22;
        int TLENGTH = 6;
        String stDT = "";
        switch (TrafficType) {
            case 1://low
                if(strTraffic.length()>lowTrafficIndex+TLENGTH+1 ){
                    stDT = strTraffic.substring(lowTrafficIndex, lowTrafficIndex + TLENGTH);}
                break;
            case 2://mid
                if(strTraffic.length()>midTrafficIndex+TLENGTH+1 ){
                    stDT = strTraffic.substring(midTrafficIndex, midTrafficIndex + TLENGTH);}
                break;
            case 3://high
                if(strTraffic.length()>highTrafficIndex+TLENGTH+1 ){
                    stDT = strTraffic.substring(highTrafficIndex, highTrafficIndex + TLENGTH);}
                break;
        }
        return stDT;
    }
}
