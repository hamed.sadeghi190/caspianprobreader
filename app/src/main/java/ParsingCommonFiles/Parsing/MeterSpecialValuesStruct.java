package ParsingCommonFiles.Parsing;



public class MeterSpecialValuesStruct {
    private String dateTime = ConstDefines.DASH_CONSTANT;
    private String phaseVoltage1 = ConstDefines.DASH_CONSTANT;
    private String phaseVoltage2 = ConstDefines.DASH_CONSTANT;
    private String phaseVoltage3 = ConstDefines.DASH_CONSTANT;
    private String phaseCurrent1 = ConstDefines.DASH_CONSTANT;
    private String phaseCurrent2 = ConstDefines.DASH_CONSTANT;
    private String phaseCurrent3 = ConstDefines.DASH_CONSTANT;
    private String powerFactor1 = ConstDefines.DASH_CONSTANT;
    private String powerFactor2 = ConstDefines.DASH_CONSTANT;
    private String powerFactor3 = ConstDefines.DASH_CONSTANT;

    public void initial(){
        setDateTime("");
        setPhaseVoltage1("");
        setPhaseVoltage2("");
        setPhaseVoltage3("");
        setPhaseCurrent1("");
        setPhaseCurrent2("");
        setPhaseCurrent3("");
        setPowerFactor1("");
        setPowerFactor2("");
        setPowerFactor3("");
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public void setPhaseVoltage1(String phaseVoltage1) {
        this.phaseVoltage1 = phaseVoltage1;
    }

    public void setPhaseVoltage2(String phaseVoltage2) {
        this.phaseVoltage2 = phaseVoltage2;
    }

    public void setPhaseVoltage3(String phaseVoltage3) {
        this.phaseVoltage3 = phaseVoltage3;
    }

    public void setPhaseCurrent1(String phaseCurrent1) {
        this.phaseCurrent1 = phaseCurrent1;
    }

    public void setPhaseCurrent2(String phaseCurrent2) {
        this.phaseCurrent2 = phaseCurrent2;
    }

    public void setPhaseCurrent3(String phaseCurrent3) {
        this.phaseCurrent3 = phaseCurrent3;
    }

    public void setPowerFactor1(String powerFactor1) {
        this.powerFactor1 = powerFactor1;
    }

    public void setPowerFactor2(String powerFactor2) {
        this.powerFactor2 = powerFactor2;
    }

    public void setPowerFactor3(String powerFactor3) {
        this.powerFactor3 = powerFactor3;
    }

    private String reviseOutputString(String inputString)
    {
        if(inputString.length() > 20)
            return inputString.substring(0,20);
        return inputString;
    }

    public String getPhaseVoltage1() { return this.phaseVoltage1 ; }

    public String getPhaseVoltage2() {
        return this.phaseVoltage2;
    }

    public String getPhaseVoltage3() {
        return this.phaseVoltage3 ;
    }

    public String getPhaseCurrent1() {
        return this.phaseCurrent1 ;
    }

    public String getPhaseCurrent2() {
        return this.phaseCurrent2 ;
    }

    public String getPhaseCurrent3() {
        return this.phaseCurrent3 ;
    }

    public String getPowerFactor1() {
        return this.powerFactor1 ;
    }

    public String getPowerFactor2() {
        return this.powerFactor2 ;
    }

    public String getPowerFactor3() {
        return this.powerFactor3 ;
    }

    public String getString(){
        String retVal = "\t\tDate Time = " + reviseOutputString(dateTime) + "\n";
        retVal += "\t\tPhase Voltage 1 = " + reviseOutputString(phaseVoltage1) +"\n";
        retVal += "\t\tPhase Voltage 2 = " + reviseOutputString(phaseVoltage2) +"\n";
        retVal += "\t\tPhase Voltage 3 = " + reviseOutputString(phaseVoltage3) +"\n";
        retVal += "\t\tPhase Current 1 = " + reviseOutputString(phaseCurrent1) +"\n";
        retVal += "\t\tPhase Current 2 = " + reviseOutputString(phaseCurrent2) +"\n";
        retVal += "\t\tPhase Current 3 = " + reviseOutputString(phaseCurrent3) +"\n";
        retVal += "\t\tPower Factor 1 = " + reviseOutputString(powerFactor1) +"\n";
        retVal += "\t\tPower Factor 2 = " + reviseOutputString(powerFactor2) +"\n";
        retVal += "\t\tPower Factor 3 = " + reviseOutputString(powerFactor3) +"\n";
        return  retVal;
    }
}
