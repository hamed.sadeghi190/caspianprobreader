package ParsingCommonFiles.Parsing;



public class ConstDefines {

    public static int DEFAULT_PREFIX_SIZE_SIX = 6;
    public static int DEFAULT_PREFIX_SIZE_SEVEN = 7;
    public static int DEFAULT_PREFIX_SIZE_EIGHT = 8;
    public static int DEFAULT_PREFIX_SIZE_TEN = 10;
    public static int DEFAULT_PREFIX_SIZE_Eleven = 12;

    public static final int DEFAULT_INITIAL_VALUE = -1;

    public static final int FULL_READ_MODE = 1;
    public static final int CHALLENGE_RESPONSE_READ_MODE = 0;
    public static final int CHALLENGE_READ_SERIAL = 1;
    public static final int CHALLENGE_READ_DATE_TIME = 2;
    public static final int CHALLENGE_READ_POWERS = 3;

    public static final int METER_TIME_SIZE = 8;
    public static final int METER_DATE_SIZE = 8;
    public static final int METER_SERIAL_SIZE = 8;

    public static final int K_WATT_CONSTANT  = 0;
    public static final int WATT_CONSTANT  = 1;

    public static final String DASH_CONSTANT = "-";
    public static final String KILO_WATT_CONSTANT = "*kW)";
    public static final String KILO_WATT_HOUR_CONSTANT = "*kWh)";
    public static final String WATT_HOUR_CONSTANT = "*Wh)";
    public static final String KILO_VAR_HOUR_CONSTANT = "*kvarh)";

    public static final String ELSTER_A220_NAME =                   "/ABB5\\@V7.00";
    public static final String ELSTER_A1350_NAME =                  "/ABB5\\@V5.00";
    public static final String ELSTER_A1350_NAME1 =                 "/ABB5\\@V5.20";
    public static final String ELSTER_A1350_NAME2 =                 "/ABB5\\@V5";
    public static final String ELSTER_A1440_NAME =                  "/ABB5\\@V9.20";
    public static final String ELSTER_A1500_NAME =                  "/ABB5\\@V4.20";
    public static final String LANDIS_PUBLIC_NAME1 =                "ZMG310";
    public static final String LANDIS_PUBLIC_NAME2 =                "ZMG410";
    public static final String LANDIS_PUBLIC_NAME3 =                "ZMG405";
    public static final String LANDIS_NAME =                        "/LGZ6\\2ZMG310000b.P03";
    public static final String LANDIS_NAME2 =                       "/LGZ6\\2ZMG310000b.P04";
    public static final String LANDIS_NAME3 =                       "/LGZ5\\2ZMG410041b.P05";
    public static final String LANDIS_NAME4 =                       "/LGZ5\\2ZMG310000b.P03";
    public static final String LANDIS_NAME5 =                       "/LGZ6\\2ZMG410440a.P03";
    public static final String LANDIS_NAME6 =                       "/LGZ6\\2ZMG410000b.P04";
    public static final String JAM200_NAME =                        "/EAA5JAM200";
    public static final String JAM200T_NAME =                        "/EAA5JM200T";
    public static final String JAM_300_NAME =                       "/EAA5JAM300";
    public static final String JAM_300_NEW_VERSION2_TAVANIR_NAME =  "/EAA5JM300X";
    public static final String ISKRA_NAME =                         "/ISKCMT3xH-H15-V1.1";
    public static final String KTC_NAME =                           "EMS210";
    public static final String KTC430_NAME =                        "EM430";
    public static final String RENAN_2000_NAME =                    "/HXE3\\3";
    public static final String HEX_12_NAME =                        "/NEG4HX";
    public static final String IRANTEK_12_NAME =                    "/HXE4\\4";
    public static final String IRANTEK_34_NAME =                    "/HXE4\\4";
    public static final String HEX_34_NAME =                        "/HXE4\\4";
    public static final String HEX_300_NAME =                       "/HXE5\\2HXE310";
    public static final String JAM_110_NAME =                       "A4EAA kWh Meter";
    public static final String JAM_110_NAME1 =                      "A5EAA kWh Meter";
    public static final String JAM_110_NAME2 =                      "EAA0EAA kWh Meter";
    public static final String JAM_110_NAME3 =                      "5EAA kWh Meter";
    public static final String ACE_2000_NAME =                      "/ACE5SMIRAN";
    public static final String ACE_5000_NAME =                      "/SLb5\\2ACE5K51";
    public static final String ACE_6000_NAME =                      "/ACE5\\2ACE6000_R3";
    public static final String SNH_NAME =                           "/RLN50010A51.00VS2.02";
    public static final String TFC_NAME =                           "/TFC4/";
    public static final String EVAN1010_NAME =                      "/SLFZ";
    public enum MeterTypes{
            UNDEFINED,
        ELSTER_A220,
        ELSTER_A1350,
        ELSTER_A1440,
        ELSTER_A1500,
        LANDIS,
        JAM_200,
        JAM_200T,
        JAM_300,
        JAM_300_newVersion2Tavanir,
        ISKRA,
        KTC,
        RENAN_2000,
        IRANTEK_12,
        IRANTEK_34,
        HEX_12,
        HEX_34,
        HEX_300,
        HEX_400,
        JAM_110,
        ACE_5000,
        ACE_2000,
        KTC430,
        SNH,
        TFC200,
        TFC200F,
        EVAN_1010
    };
}
