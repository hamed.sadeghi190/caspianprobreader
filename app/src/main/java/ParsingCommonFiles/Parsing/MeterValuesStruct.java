package ParsingCommonFiles.Parsing;



public class MeterValuesStruct {
    private String meterSerial = ConstDefines.DASH_CONSTANT;
    private String meterPowerActiveT3 = ConstDefines.DASH_CONSTANT; //1.8.3
    private String meterPowerActiveT1 = ConstDefines.DASH_CONSTANT; //1.8.1
    private String meterPowerActiveT2 = ConstDefines.DASH_CONSTANT; //1.8.2
    private String meterPowerActiveT4 = ConstDefines.DASH_CONSTANT;
    private String meterMaximumDimand = ConstDefines.DASH_CONSTANT; //1.6.1
    private String meterPowerReActive = ConstDefines.DASH_CONSTANT; //3.8.0
    private String meterTime = ConstDefines.DASH_CONSTANT;
    private String meterDate = ConstDefines.DASH_CONSTANT;
    private String meterPowerOut = ConstDefines.DASH_CONSTANT;

    public void initial() {
        meterSerial = "-";
        meterPowerActiveT3 = "-";
        meterPowerActiveT1 = "-";
        meterPowerActiveT2 = "-";
        meterPowerActiveT4 = "-";
        meterMaximumDimand = "-";
        meterPowerReActive = "-";
        meterTime = "-";
        meterDate = "-";
        meterPowerOut = "-";
    }

    public void setMeterSerial(String meterSerial) {
        this.meterSerial = meterSerial;
    }

    public void setMeterPowerActiveT3(String meterPowerActiveT3) {
        this.meterPowerActiveT3 = meterPowerActiveT3;
    }

    public void setMeterPowerActiveT1(String meterPowerActiveT1) {
        this.meterPowerActiveT1 = meterPowerActiveT1;
    }

    public void setMeterPowerActiveT2(String meterPowerActiveT2) {
        this.meterPowerActiveT2 = meterPowerActiveT2;
    }

    public void setMeterPowerActiveT4(String meterPowerActiveT4) {
        this.meterPowerActiveT4 = meterPowerActiveT4;
    }

    public void setMeterMaximumDimand(String meterMaximumDimand) {
        this.meterMaximumDimand = meterMaximumDimand;
    }

    public void setMeterPowerReActive(String meterPowerReActive) {
        this.meterPowerReActive = meterPowerReActive;
    }

    public void setMeterTime(String meterTime) {
        this.meterTime = meterTime;
    }

    public void setMeterDate(String meterDate) {
        this.meterDate = meterDate;
    }

    public void setMeterPowerOut(String meterPowerOut) {
        this.meterPowerOut = meterPowerOut;
    }

    public String reviseOutputString(String inputString)
    {
        if(inputString.length() > 20)
            return inputString.substring(0,20);
        return inputString;
    }

    public String getMeterSerial(){
       return meterSerial;
    }
    public int getPowerActiveT1(){
        if(!meterPowerActiveT1.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterPowerActiveT1);
        return 0;
    }
    public int getPowerActiveT2(){
        if(!meterPowerActiveT2.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterPowerActiveT2);
        return 0;
    }
    public int getPowerActiveT3(){
        if(!meterPowerActiveT3.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterPowerActiveT3);
        return 0;
    }
    public int getPowerActiveT4(){
        if(!meterPowerActiveT4.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterPowerActiveT4);
        return 0;
    }
    public int getMaximumDemand(){
        if(!meterMaximumDimand.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterMaximumDimand);
        return 0;
    }
    public int getReactive(){
        if(!meterPowerReActive.equals(ConstDefines.DASH_CONSTANT))
            return Integer.parseInt(meterPowerReActive);
        return 0;
    }
    public int getSum(){
        return getPowerActiveT1()+getPowerActiveT2()+getPowerActiveT3();
    }

    public Double getPowerActiveMidDouble(){
        if(!meterPowerActiveT1.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble (meterPowerActiveT1);
        return 0.0;
    }
    public Double getPowerActiveTopDouble(){
        if(!meterPowerActiveT2.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble(meterPowerActiveT2);
        return 0.0;
    }
    public Double getPowerActiveBottomDouble(){
        if(!meterPowerActiveT3.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble(meterPowerActiveT3);
        return 0.0;
    }
    public Double getPowerActiveFridayDouble(){
        if(!meterPowerActiveT4.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble(meterPowerActiveT4);
        return 0.0;
    }
    public Double getMaximumDemandDouble(){
        if(!meterMaximumDimand.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble(meterMaximumDimand);
        return 0.0;
    }
    public Double getReactiveDouble(){
        if(!meterPowerReActive.equals(ConstDefines.DASH_CONSTANT))
            return Double.parseDouble(meterPowerReActive);
        return 0.0;
    }
    public Double getSumDouble(){
        return getPowerActiveMidDouble()+getPowerActiveTopDouble()+getPowerActiveBottomDouble();
    }


    public String getPowerActiveMid(){
        if(!meterPowerActiveT1.equals(ConstDefines.DASH_CONSTANT))
            return meterPowerActiveT1;
        return "0";
    }
    public String getPowerActiveTop(){
        if(!meterPowerActiveT2.equals(ConstDefines.DASH_CONSTANT))
            return meterPowerActiveT2;
        return "0";
    }
    public String getPowerActiveBottom(){
        if(!meterPowerActiveT3.equals(ConstDefines.DASH_CONSTANT))
            return meterPowerActiveT3;
        return "0";
    }
    public String getPowerActiveFriday(){
        if(!meterPowerActiveT4.equals(ConstDefines.DASH_CONSTANT))
            return meterPowerActiveT4;
        return "0";
    }
    public String getMaximumDemandStr(){
        if(!meterMaximumDimand.equals(ConstDefines.DASH_CONSTANT))
            return meterMaximumDimand;
        return "0";
    }
    public String getReactiveStr(){
        if(!meterPowerReActive.equals(ConstDefines.DASH_CONSTANT))
            return meterPowerReActive;
        return "0";
    }

    public String getString(){
        String retVal = "\t\tSerial = " + reviseOutputString(meterSerial) + "\n";
        retVal += "\t\tDate = " + reviseOutputString(meterDate) +"\n";
        retVal += "\t\tTime = " + reviseOutputString(meterTime) +"\n";
        retVal += "\t\tPower Active 1 = " + reviseOutputString(meterPowerActiveT1) +"\n";
        retVal += "\t\tPower Active 2 = " + reviseOutputString(meterPowerActiveT2) +"\n";
        retVal += "\t\tPower Active 3 = " + reviseOutputString(meterPowerActiveT3) +"\n";
        retVal += "\t\tPower Active 4 = " + reviseOutputString(meterPowerActiveT4) +"\n";
        retVal += "\t\tPower ReActive = " + reviseOutputString(meterPowerReActive) +"\n";
        retVal += "\t\tMaximum Demand = " + reviseOutputString(meterMaximumDimand) +"\n";
        retVal += "\t\tPowerOut = " + reviseOutputString(meterPowerOut) +"\n";
        return  retVal;
    }

    public  String GetSerial()
    {
       return reviseOutputString(meterSerial);
    }

    public  String GetMaxiMeter()
    {
        return reviseOutputString(meterMaximumDimand);
    }
    public  String GetRactive()
    {
        return reviseOutputString(meterPowerReActive);
    }
    public  String getPowerlow()
    {
        return  reviseOutputString(meterPowerActiveT1);
    }

    public  String getPowerMidd()
    {
        return  reviseOutputString(meterPowerActiveT2);
    }
    public  String getPowerhigh()
    {
        return  reviseOutputString(meterPowerActiveT3);
    }
    public  String getdate()
    {
        return  reviseOutputString(meterDate);
    }

    public  String gettime()
    {
        return  reviseOutputString(meterTime);
    }


}
