package ParsingCommonFiles.Parsing;

import ParsingCommonFiles.Meters._ACE_2000;
import ParsingCommonFiles.Meters._ACE_5000;
import ParsingCommonFiles.Meters._ELSTER_A1350;
import ParsingCommonFiles.Meters._ELSTER_A1440;
import ParsingCommonFiles.Meters._ELSTER_A1500;
import ParsingCommonFiles.Meters._ELSTER_A220;
import ParsingCommonFiles.Meters._EVAN_1010;
import ParsingCommonFiles.Meters._HEX_12;
import ParsingCommonFiles.Meters._HEX_300;
import ParsingCommonFiles.Meters._HEX_34;
import ParsingCommonFiles.Meters._ISKRA;
import ParsingCommonFiles.Meters._JAM_110;
import ParsingCommonFiles.Meters._JAM_200;
import ParsingCommonFiles.Meters._JAM_200T;
import ParsingCommonFiles.Meters._JAM_300;
import ParsingCommonFiles.Meters._JAM_300_newVersion2Tavanir;
import ParsingCommonFiles.Meters._KTC;
import ParsingCommonFiles.Meters._KTC430;
import ParsingCommonFiles.Meters._LANDIS;
import ParsingCommonFiles.Meters._Meter_Base;
import ParsingCommonFiles.Meters._RENAN_2000;
import ParsingCommonFiles.Meters._SNH;
import ParsingCommonFiles.Meters._TFC200;
import ParsingCommonFiles.Meters._TFC200F;

import static ParsingCommonFiles.Parsing.ConstDefines.ACE_2000_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ACE_5000_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A1350_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A1350_NAME1;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A1350_NAME2;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A1440_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A1500_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ELSTER_A220_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.EVAN1010_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.HEX_12_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.HEX_300_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.HEX_34_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.IRANTEK_12_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.IRANTEK_34_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.ISKRA_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM200_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_110_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_110_NAME1;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_110_NAME2;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_110_NAME3;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_300_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.JAM_300_NEW_VERSION2_TAVANIR_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.KILO_WATT_HOUR_CONSTANT;
import static ParsingCommonFiles.Parsing.ConstDefines.KTC430_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.KTC_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.K_WATT_CONSTANT;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME2;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME3;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME4;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME5;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_NAME6;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_PUBLIC_NAME1;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_PUBLIC_NAME2;
import static ParsingCommonFiles.Parsing.ConstDefines.LANDIS_PUBLIC_NAME3;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.ACE_2000;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.TFC200;
import static ParsingCommonFiles.Parsing.ConstDefines.MeterTypes.TFC200F;
import static ParsingCommonFiles.Parsing.ConstDefines.RENAN_2000_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.SNH_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.TFC_NAME;
import static ParsingCommonFiles.Parsing.ConstDefines.WATT_CONSTANT;



public class ParseReceivedData {

    private MeterTypes meterType_;
    private String rawDataDefault;
    private String rawDataSpecial;
    public boolean isExceptionOccurred = false;
    public String occurredExceptionText = "";

    public void setMeterType_(MeterTypes meterType_) {
        this.meterType_ = meterType_;
    }

    public void setRawDataDefault(String rawDataDefault) {
        this.rawDataDefault = rawDataDefault;
    }

    public void setRawDataSpecial(String rawDataSpecial) {
        this.rawDataSpecial = rawDataSpecial;
    }

    public ParseReceivedData(){

    }

    public boolean parseMeter(MeterValuesStruct meterValuesStruct, MeterSpecialValuesStruct meterSpecialValuesStruct){
        _Meter_Base meter = new _Meter_Base();
        meter.resetStructuresFields();
        switch (meterType_) {
            case ACE_2000:
                meter = new _ACE_2000();
                break;
            case ACE_5000:
                meter = new _ACE_5000();
                break;
            case ELSTER_A220:
                meter = new _ELSTER_A220();
                break;
            case ELSTER_A1350:
                meter = new _ELSTER_A1350();
                break;
            case ELSTER_A1440:
                meter = new _ELSTER_A1440();
                break;
            case ELSTER_A1500:
                meter = new _ELSTER_A1500();
                break;
            case HEX_12:
                meter = new _HEX_12();
                break;
            case HEX_34:
                meter = new _HEX_34();
                break;
            case HEX_300:
                meter = new _HEX_300();
                break;
            case ISKRA:
                meter = new _ISKRA();
                break;
            case JAM_110:
                meter = new _JAM_110();
                break;
            case JAM_200:
                meter = new _JAM_200();
                break;
            case JAM_200T:
                meter = new _JAM_200T();
                break;
            case JAM_300:
                meter = new _JAM_300();
                break;
            case JAM_300_newVersion2Tavanir:
                meter = new _JAM_300_newVersion2Tavanir();
                break;
            case KTC:
                meter = new _KTC();
                break;
            case KTC430:
                meter = new _KTC430();
                break;
            case LANDIS:
                meter = new _LANDIS();
                break;
            case RENAN_2000:
                meter = new _RENAN_2000();
                break;
            case SNH:
                meter = new _SNH();
                break;
            case TFC200:
                meter = new _TFC200();
                break;
            case TFC200F:
                meter = new _TFC200F();
                break;
            case EVAN_1010:
                meter = new _EVAN_1010();
                break;
        }

        try{
            meter.initial();
            if(meterType_ != ACE_2000) {
                if ( meterType_ != TFC200 ) { // A L L  T Y P E S  O F  M E T E R S
                    meter.parse(rawDataDefault, meterValuesStruct, K_WATT_CONSTANT);
                }else // M E T E R  I S  T F C 2 0 0
                {
                    rawDataDefault = rawDataDefault.replace("*kwh)", KILO_WATT_HOUR_CONSTANT);
                    meter.parse(rawDataDefault, meterValuesStruct, K_WATT_CONSTANT);
                }
            }
            else { // M E T E R  I S  A C E _2 0 0 0
                meter.parse(rawDataDefault, meterValuesStruct, WATT_CONSTANT);
            }
            if(rawDataSpecial != "")
                meter.parse(rawDataSpecial, meterSpecialValuesStruct);
        }catch (IndexOutOfBoundsException e)
        {
            isExceptionOccurred = true;
            occurredExceptionText = "L1:"+rawDataDefault.length()+"\n"+
                    "L2:"+rawDataSpecial.length()+"\n"+
                    " Index Out Of Bounds Exception! \n"+ e.getMessage();
            System.err.println("IndexOutOfBoundsException: " + e.getMessage());
            return false;
        }
        catch (Exception exp)
        {
            isExceptionOccurred = true;
            occurredExceptionText = "An Exception Occurred! \n"+ exp.getMessage();
            return false;
        }
        return true;
    }

    public void reset() {
        meterType_ = MeterTypes.UNDEFINED;
        rawDataDefault = "";
        rawDataSpecial = "";
        isExceptionOccurred = false;
        occurredExceptionText = "";
    }

    public MeterTypes getMeterTypeAndTipFromReadData(String meterReadString) {
        MeterTypes retval = MeterTypes.UNDEFINED;
        if(meterReadString.contains(JAM200_NAME))
            return MeterTypes.JAM_200;
        else if(meterReadString.contains(ELSTER_A220_NAME))
            return MeterTypes.ELSTER_A220;
        else if(meterReadString.contains(ELSTER_A1350_NAME) ||
                meterReadString.contains(ELSTER_A1350_NAME1) ||
                meterReadString.contains(ELSTER_A1350_NAME2))
            return MeterTypes.ELSTER_A1350;
        else if(meterReadString.contains(ELSTER_A1440_NAME))
            return getElesterTip(MeterTypes.ELSTER_A1440, meterReadString);
        else if(meterReadString.contains(ELSTER_A1500_NAME))
            return MeterTypes.ELSTER_A1500;
        else if(meterReadString.contains(LANDIS_PUBLIC_NAME1) ||
                meterReadString.contains(LANDIS_PUBLIC_NAME2) ||
                meterReadString.contains(LANDIS_PUBLIC_NAME3) ||
                meterReadString.contains(LANDIS_NAME) ||
                meterReadString.contains(LANDIS_NAME2) ||
                meterReadString.contains(LANDIS_NAME3) ||
                meterReadString.contains(LANDIS_NAME4) ||
                meterReadString.contains(LANDIS_NAME5) ||
                meterReadString.contains(LANDIS_NAME6) )
            return MeterTypes.LANDIS;
        else if(meterReadString.contains(JAM_300_NAME))
            return MeterTypes.JAM_300;
        else if(meterReadString.contains(JAM_300_NEW_VERSION2_TAVANIR_NAME))
            return MeterTypes.JAM_300_newVersion2Tavanir;
        else if(meterReadString.contains(ISKRA_NAME))
            return MeterTypes.ISKRA;
        else if(meterReadString.contains(EVAN1010_NAME))
            return MeterTypes.EVAN_1010;
        else if(meterReadString.contains(KTC_NAME))
            return MeterTypes.KTC;
        else if(meterReadString.contains(KTC430_NAME))
            return MeterTypes.KTC430;
        else if(meterReadString.contains(RENAN_2000_NAME))
            return MeterTypes.RENAN_2000;
        else if(meterReadString.contains(HEX_12_NAME))
            return MeterTypes.HEX_12;
        else if(meterReadString.contains(HEX_300_NAME))
            return MeterTypes.HEX_300;
        else if(meterReadString.contains(JAM_110_NAME)||
                meterReadString.contains(JAM_110_NAME1)||
                meterReadString.contains(JAM_110_NAME2)||
                meterReadString.contains(JAM_110_NAME3))
            return MeterTypes.JAM_110;
        else if(meterReadString.contains(ACE_5000_NAME))
            return MeterTypes.ACE_5000;
        else if(meterReadString.contains(ACE_2000_NAME))
            return MeterTypes.ACE_2000;
        else if(meterReadString.contains(SNH_NAME))
            return MeterTypes.SNH;
        else if(meterReadString.contains(TFC_NAME))
        {
            if(meterReadString.contains("01.08.01"))
                return TFC200F;
            else if(meterReadString.contains("0.0.1("))
                return MeterTypes.TFC200;
        }
        else if(meterReadString.contains(IRANTEK_12_NAME) ||
                meterReadString.contains(IRANTEK_34_NAME)||
                meterReadString.contains(HEX_34_NAME))
        {
            if(meterReadString.contains("1-0:0.0.0.255("))
                return MeterTypes.IRANTEK_34;
            else
                return MeterTypes.HEX_34;
        }

        return retval;
    }

    public MeterTypes getElesterTip(MeterTypes meterType, String meterReadString) {
        return MeterTypes.ELSTER_A1440;
    }

}
